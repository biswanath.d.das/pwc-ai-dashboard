const OPENAI_BASE_URL = `https://soumenopenai.openai.azure.com/openai/deployments/gpt-35-turbo/chat/completions?api-version=2023-03-15-preview`;
const API_KEY = "3a5a6eba4d2546558d3fa749ef9fb5ce";
const authority = "soumenopenai.openai.azure.com";
const genetatePrompt = (name) => {
  return `suggest a list of medicine and itz compositions for the treatment of ${name} 
    Poins must be consider for reply:          
    - Reply only a json for only          
    Expeted output format          
    [  {  name:"medicine 1", compositions:["c1", "c2", "c3"] },{name:"medicine 2",compositions:["c1", "c2", "c3"]}, ....]
    `;
};

const callToOpenAI = async (name) => {
  var myHeaders = new Headers();
  myHeaders.append("authority", authority);
  myHeaders.append("accept", "*/*");
  myHeaders.append("accept-language", "en-GB,en-US;q=0.9,en;q=0.8");
  myHeaders.append("api-key", API_KEY);
  myHeaders.append("authorization", "Bearer a8fe99cb6b354a06913f189536cdf8fc");
  myHeaders.append("content-type", "application/json");
  myHeaders.append(
    "sec-ch-ua",
    '"Not/A)Brand";v="99", "Google Chrome";v="115", "Chromium";v="115"'
  );
  myHeaders.append("sec-ch-ua-mobile", "?0");
  myHeaders.append("sec-ch-ua-platform", '"Windows"');
  myHeaders.append("sec-fetch-dest", "empty");
  myHeaders.append("sec-fetch-mode", "cors");
  myHeaders.append("sec-fetch-site", "cross-site");
  myHeaders.append(
    "user-agent",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
  );

  var raw = JSON.stringify({
    messages: [
      {
        role: "system",
        content: "You are a doctor",
      },
      {
        role: "user",
        content: genetatePrompt(name),
      },
    ],
    max_tokens: 800,
    temperature: 0.8,
    frequency_penalty: 0,
    presence_penalty: 0,
    top_p: 0.95,
    stop: null,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  await fetch(OPENAI_BASE_URL, requestOptions)
    .then((response) => response.json())
    .then((result) => retrun(result))
    .catch((error) => retrun(error));
};
