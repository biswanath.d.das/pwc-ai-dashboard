import { ChatInteraction } from '../../types'
import Mock from '../Mock/mock_1_GPT.json'
import MockIncomplete from '../Mock/inComplete_mock.json'
import swal from 'sweetalert'
import { OPEN_AI_COMPLETIONS_DAVINCI_API } from '../../config'
function isValidJson(str: string) {
  try {
    JSON.parse(str)
    return true
  } catch (e) {
    return false
  }
}

export async function queryTextDavinci003Completions(
  prompt: string,
  options: { apikey: string; model: string },
): Promise<ChatInteraction[] | undefined> {
  const gptData = localStorage.getItem('gpt-data')
  const analyzer_settings = localStorage.getItem('analyzer-settings')

  if (analyzer_settings) {
    const settings = JSON.parse(analyzer_settings)
    if (settings?.mock === 'true') {
      console.log('Mock data')

      const chat = [
        {
          reply: Mock.choices?.[0]?.text || '',
        },
      ]
      localStorage.setItem('gpt-data', JSON.stringify(chat))
      return chat
    } else {
      if (gptData) {
        console.log('Cashes data')
        return JSON.parse(gptData)
      } else {
        return fetch(OPEN_AI_COMPLETIONS_DAVINCI_API, {
          headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${options.apikey}`,
          },
          method: 'POST',
          body: JSON.stringify({
            max_tokens: 2000,
            model: 'text-davinci-003',
            stop: ['Human:', 'AI:'],
            temperature: 0.3,
            prompt,
          }),
        })
          .then((response) => response.json())
          .then((resp) => {
            if (resp?.error) {
              swal(
                'Warning!',
                resp?.error.message,
                'warning',
              ).then((value) => {})
              return
            }

            if (!isValidJson(resp.choices?.[0]?.text)) {
              swal(
                'Warning!',
                'Somthing wrong with the server, please Re-Analyze',
                'warning',
              ).then((value) => {})
              return
            }
            const chat = [
              {
                reply: resp.choices?.[0]?.text || '',
              },
            ]
            localStorage.setItem('gpt-data', JSON.stringify(chat))
            return chat
          })
          .catch((resp) => {
            swal('Warning!', resp?.error.message, 'warning').then((value) => {})
            const chat = [
              {
                reply: Mock.choices?.[0]?.text || '',
              },
            ]
            return chat
          })
      }
    }
  } else {
    return undefined
  }
}
