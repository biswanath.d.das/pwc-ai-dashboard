function limitContextLength(text: string, maxLength: number) {
  const truncatedTokens = text.slice(0, maxLength)

  return truncatedTokens
}

function filterDataFn(data: any) {
  if (data.length > 50) {
    // Find the lowest and highest valued data in the 'y' property
    let minData = data.reduce(
      (min: any, item: any) => (item.y < min ? item.y : min),
      data[0].y,
    )
    let maxData = data.reduce(
      (max: any, item: any) => (item.y > max ? item.y : max),
      data[0].y,
    )

    // Filter out data to get only the lowest and highest values
    let filteredData = data.filter(
      (item: any) => item.y === minData || item.y === maxData,
    )
    return filteredData
  } else {
    return data
  }
}

export const filterContextData = (reportData: any) => {
  const filteredData = reportData.map((item: any) => ({
    ...item,
    data: filterDataFn(item.data),
  }))
  const filterData = limitContextLength(JSON.stringify(filteredData), 7000)
  return filterData
}
