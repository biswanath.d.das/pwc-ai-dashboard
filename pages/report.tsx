import * as React from "react";
import GenerateReport from "../components/viz/GenerateReport";
import PageLayout from "../components/layout/PageLayout";
export default function manual() {
  return <PageLayout Body={() => <GenerateReport />} />;
}
