import "../styles/globals.css";
import type { AppProps } from "next/app";
import React, { useState, createContext, useContext, useEffect } from "react";

const CoreContext = createContext<any>(null);
const ChatContext = createContext<any>([]);
const ReportContext = createContext<any>([]);

export const useCoreData = () => useContext(CoreContext);
export const useChatData = () => useContext(ChatContext);
export const useReportData = () => useContext(ReportContext);

export default function App({ Component, pageProps }: AppProps) {
  const [coreData, setCoreData] = useState(null);
  const [reportData, setReportData] = useState([]);
  const [chatData, setChatData] = useState([]);

  useEffect(() => {
    const handleBeforeUnload = (event: any) => {
      event.preventDefault();
      event.returnValue = ""; // Modern browsers require a returnValue to show the alert
    };

    const addBeforeUnloadListener = () => {
      window.addEventListener("beforeunload", handleBeforeUnload);
    };

    const removeBeforeUnloadListener = () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };

    addBeforeUnloadListener();

    return () => {
      removeBeforeUnloadListener();
    };
  }, []);

  return (
    <CoreContext.Provider value={{ coreData, setCoreData }}>
      <ReportContext.Provider value={{ reportData, setReportData }}>
        <ChatContext.Provider value={{ chatData, setChatData }}>
          <Component {...pageProps} />
        </ChatContext.Provider>
      </ReportContext.Provider>
    </CoreContext.Provider>
  );
}
