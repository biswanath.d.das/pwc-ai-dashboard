import Head from "next/head";
import React, { useState, createContext, useContext } from "react";
import {
  PanelContent,
  WelcomeHeader,
  Dashboard,
  DataLoadedMessage,
  Button,
  Panel,
} from "../components";
import { generateDashboard } from "../openai";
import { IDashboard, IDataset, ISettings } from "../types";
import { parseData } from "../utils/parseData";
import PageLayout from "../components/layout/PageLayout";
import { useRouter } from "next/router";
import swal from "sweetalert";
import Chat from "../components/viz/Chat";
import { useCoreData } from "./_app";
import Image from "next/image";

export default function Home() {
  const [settings, setSettings] = React.useState<ISettings>({
    apikey: "",
    sampleRows: 1,
    model: "",
    mock: "",
    context: "",
  });
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<IDataset>();
  const [userContext, setUserContext] = React.useState<string>("");
  const [dashboard, setDashboard] = React.useState<IDashboard | null>();
  let { coreData } = useCoreData();
  const [setKey, setNoKey] = useState(false);

  const router = useRouter();
  React.useEffect(() => {
    localStorage.setItem("chat_base_data", JSON.stringify([]));

    const config = localStorage.getItem("analyzer-settings");
    console.log(config, "config---");
    console.log(coreData, "coreDatacheck");

    if (config) {
      setSettings(JSON.parse(config) as ISettings);
      setNoKey(true);
    }
    if (coreData) {
      setData(coreData);
    }

    setDashboard(null);
  }, []);

  const handleAnalyze = React.useCallback(() => {
    setLoading(true);
    const config = localStorage.getItem("analyzer-settings");
    const settingData = config && JSON.parse(config);
    if (!settingData?.apikey) {
      setNoKey(false);
      // swal("Warning!", "Please add API key", "info").then((value) => {
      //   router.push("/settings");
      // });
      setLoading(false);
    } else if (data) {
      setNoKey(true);
      generateDashboard(
        data,
        userContext,
        settings.sampleRows,
        settings.apikey,
        settings.model
      )
        .then((response) => {
          setDashboard(response.dashboard);
          setLoading(false);
        })
        .catch((err) => {
          setDashboard(null);
          setLoading(false);
        });
    }
    setLoading(false);
  }, [data, userContext, settings]);

  React.useEffect(() => {
    handleAnalyze();
  }, [settings]);

  const reAnalize = () => {
    localStorage.removeItem("gpt-data");
    handleAnalyze();
  };

  return (
    <>
      <PageLayout
        Body={() => (
          <>
            {/* <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-2 in-ct-ht">
              <div>
                <h2 className="mb-1">AI enabled data analitics tool</h2>
              </div>
              <div>
                {data ? (
                  <div style={{ display: "flex" }}>
                    <DataLoadedMessage onAnalyze={reAnalize} />
                    <Chat />
                  </div>
                ) : (
                  "AI enabled data analitics tool"
                )}
              </div>
            </div> */}

            {!data && (
              <div className="center">
                <div
                  className="center-button-wrapper"
                  onClick={() =>
                    setKey ? router.push("/addData") : router.push("/settings")
                  }
                >
                  <button className="in-btn-cmn me-2 in-btn-p50  m-5">
                    {/* <span className="me-2">
                <Image src={icon_download} alt="" />
              </span>{" "} */}
                    {setKey ? "Upload New Data" : "Add API Key"}
                  </button>
                </div>
              </div>
            )}

            {loading && (
              <center>
                <div className="center">
                  <div className="center-button-wrapper">
                    <div className="loader"></div>
                    <br />
                    Analyzing data...
                  </div>
                </div>
              </center>
            )}

            {!loading && dashboard && data ? (
              <Dashboard
                data={data}
                dashboard={dashboard}
                onAnalyze={reAnalize}
              />
            ) : null}
          </>
        )}
      />
    </>
  );
}
