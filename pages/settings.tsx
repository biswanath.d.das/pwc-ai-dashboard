import Head from "next/head";
import React from "react";
import {
  PanelContent,
  PanelHeader,
  WelcomeHeader,
  SettingsModal,
} from "../components";
import { Loader } from "../components/layout/Loader";
import { ISettings } from "../types";
import PageLayout from "../components/layout/PageLayout";

export default function Home() {
  const [settings, setSettings] = React.useState<ISettings>({
    apikey: "",
    sampleRows: 1,
    model: "",
    mock: "true",
    context: "",
  });

  React.useEffect(() => {
    const config = localStorage.getItem("analyzer-settings");

    if (config) {
      setSettings(JSON.parse(config) as ISettings);
    }
  }, []);

  const handleSettingsChange = React.useCallback((settings: ISettings) => {
    localStorage.setItem("analyzer-settings", JSON.stringify(settings));
    setSettings(settings);
  }, []);

  return (
    <>
      <PageLayout
        Body={() => (
          <section className="w-100 d-flex h-100 in-center-section flex-column px-4">
            <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-2 in-ct-ht">
              <div>
                <h2 className="mb-1">Settings</h2>
              </div>
            </div>
            <div className="in-mc-sec">
              <div className="in-p-sec d-flex flex-wrap">
                <SettingsModal
                  value={settings}
                  onChange={handleSettingsChange}
                />
              </div>
            </div>
          </section>
        )}
      />
    </>
  );
}
