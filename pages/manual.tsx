import * as React from "react";
import ManualAnalycys from "../components/viz/ManualAnalycys";
import PageLayout from "../components/layout/PageLayout";
export default function manual() {
  return <PageLayout Body={() => <ManualAnalycys />} />;
}
