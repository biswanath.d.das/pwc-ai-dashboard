import Head from "next/head";
import React from "react";
import {
  Button,
  ButtonsRow,
  Icon,
  PanelContent,
  PanelHeader,
  WelcomeHeader,
  ButtonIcon,
  TextInput,
  ButtonLink,
  UploadDatasetButton,
} from "../components";
import { Loader } from "../components/layout/Loader";
import { Table } from "../components/layout/Table";
import { IDataset } from "../types";
import { parseData, stringifyData } from "../utils/parseData";
import PageLayout from "../components/layout/PageLayout";
import DynamicDataTable from "../components/layout/Datatable";
import { useCoreData, useChatData, useReportData } from "./_app";
import { useRouter } from "next/router";

export default function Home() {
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<IDataset>();
  const [edit, setEdit] = React.useState(false);
  let { coreData, setCoreData } = useCoreData();
  let { reportData, setReportData } = useReportData();
  let { chatData, setChatData } = useChatData();
  const router = useRouter();

  React.useEffect(() => {
    setData(coreData);
  }, []);

  // console.log(dashboard, stringifyData(data || []));

  const handleClear = React.useCallback(() => {
    setData(undefined);
  }, []);

  const updateData = (dataset: string) => {
    setData(parseData(dataset));
  };
  const handleDatasetChange = React.useCallback((dataset: string) => {
    updateData(dataset);
  }, []);

  const resetContext = () => {
    setReportData([]);
    setChatData([]);
    localStorage.removeItem("data-summary");
    localStorage.removeItem("gpt-data");
    localStorage.removeItem("report-data");
    console.log("Reset context");
  };
  return (
    <>
      <PageLayout
        Body={() => (
          <>
            <section className="w-100 d-flex h-100 in-center-section flex-column px-4">
              <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-2 in-ct-ht">
                <div>
                  <h2 className="mb-1">My Data Collections</h2>
                </div>
                <div>
                  <button
                    className="in-btn-cmn in-btn-secondary"
                    style={{ marginRight: 20 }}
                    onClick={() => router.push("/addData")}
                  >
                    Back
                  </button>
                  <button
                    className="in-btn-cmn in-btn-primary"
                    onClick={() => setEdit(!edit)}
                  >
                    {!edit ? "Edit Data" : "Done"}
                  </button>
                </div>
              </div>
              <div className="in-mc-sec">
                <div className="in-p-sec d-flex flex-wrap">
                  {/* <Button
                    className="trash"
                    disabled={!data}
                    outline
                    onClick={handleClear}
                  >
                    <Icon icon="thrash" /> Clear
                  </Button> */}

                  <br />
                  <div
                    style={{
                      // overflowY: "auto",
                      overflow: "auto",
                      position: "absolute",
                    }}
                  >
                    {edit ? (
                      <Table
                        data={data || []}
                        onChange={(newData) => {
                          setCoreData(newData);
                          setData(newData);
                        }}
                      />
                    ) : (
                      <>{data && <DynamicDataTable data={data} />}</>
                    )}
                  </div>
                </div>
              </div>
            </section>
          </>
        )}
      />
    </>
  );
}
