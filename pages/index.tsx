import Head from "next/head";
import React from "react";
import Image from "next/image";
import Img1 from "../images/home_bg.jpg";
import Img2 from "../images/PwC_fl_wo.svg";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  const reDirect = (path: string) => {
    // router.push(path);
    window.location.replace(path);
  };
  return (
    <>
      <Head>
        <title>Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="dashboard" />

        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9"
          crossOrigin="anonymous"
        />

        {/* OG Meta tags */}
        <meta name="og:type" content="website" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <>
        <div className="hm-bg">
          <Image src={Img1} alt="" />
        </div>
        <div className="container-fluid p-0">
          <div className="hm-top d-flex justify-content-between p-3">
            <div className="hm-logo-hldr d-flex align-items-center gap-5">
              <Image src={Img2} alt="" />
              <span>VisualSense</span>
            </div>
            <div className="d-flex align-items-center hm-top-link-hldr">
              <ul className="hm-top-link d-flex">
                <li>
                  <a href="#url">About</a>
                </li>
                <li>
                  <a href="#url">Contact Us</a>
                </li>
                {/* <li>
                  <a href="#url">Login</a>
                </li> */}
              </ul>
            </div>
          </div>
          <div className="hm-content">
            <div className="hm-lh-text">
              <h1>VisualSense</h1>
              <p>
                Unlock Your Data Potential with
                <br /> VisualSense - The AI-Powered
                <br /> Dashboard Generator
              </p>
              <div className="d-flex gap-4 hm-hero-btnHdr mt-5">
                <button
                  className="hm-hero-expbtn"
                  onClick={() => router.push("/addData")}
                >
                  Get Started <span> &gt;</span>
                </button>
                {/* <button className="hm-hero-videobtn">
              <img src="images/icon_play-circle.svg" alt="" />
              <span>Watch a demo</span>
            </button> */}
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
}
