import Head from "next/head";
import React from "react";
import {
  Button,
  ButtonsRow,
  Icon,
  PanelContent,
  PanelHeader,
  WelcomeHeader,
  ButtonIcon,
  TextInput,
  ButtonLink,
  UploadDatasetButton,
} from "../components";
import { Loader } from "../components/layout/Loader";
import { Table } from "../components/layout/Table";
import { IDataset } from "../types";
import { parseData, stringifyData } from "../utils/parseData";
import PageLayout from "../components/layout/PageLayout";
import DynamicDataTable from "../components/layout/Datatable";
import { useCoreData, useChatData, useReportData } from "./_app";
import Image from "next/image";
import Script from "next/script";
import { useRouter } from "next/router";
import icon_grid from "../images/icon_grid.svg";
import icon_eye from "../images/icon_eye.svg";
import PwC_fl_c from "../images/PwC_fl_c.png";
import icon_csv2 from "../images/icon_upload-big.svg";
import icon_excel from "../images/icon_excel.svg";
import ionic_close from "../images/ionic_close.svg";

import Modal from "@mui/material/Modal";

export default function Home() {
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<IDataset>();
  const [edit, setEdit] = React.useState(false);
  let { coreData, setCoreData } = useCoreData();
  let { reportData, setReportData } = useReportData();
  let { chatData, setChatData } = useChatData();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const router = useRouter();
  const reDirect = (path: string) => {
    router.push(path);
    // window.location.replace(path);
  };
  React.useEffect(() => {
    setData(coreData);
  }, []);

  // console.log(dashboard, stringifyData(data || []));

  const handleClear = React.useCallback(() => {
    setData(undefined);
  }, []);

  const updateData = (dataset: string) => {
    setLoading(true);
    setData(parseData(dataset));
    reDirect("/data");
  };
  const handleDatasetChange = React.useCallback((dataset: string) => {
    updateData(dataset);
  }, []);

  const resetContext = () => {
    setReportData([]);
    setChatData([]);
    localStorage.removeItem("data-summary");
    localStorage.removeItem("gpt-data");
    localStorage.removeItem("report-data");
    console.log("Reset context");
  };
  // eslint-disable-next-line @next/next/no-sync-scripts, @next/next/no-sync-scripts

  return (
    <>
      <PageLayout
        Body={() => (
          <>
            <section className="w-100 d-flex h-100 in-center-section flex-column px-4">
              <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-2 in-ct-ht">
                <div>
                  <h2 className="mb-1">My Data Collections</h2>
                </div>
                <div>
                  <button
                    className="in-btn-cmn in-btn-primary"
                    onClick={handleOpen}
                  >
                    Add New Dataset
                  </button>
                </div>
              </div>

              {coreData ? (
                <div className="in-mc-sec">
                  <div className="in-p-sec d-flex flex-wrap">
                    <div className="d-flex flex-column in-pd-hldr">
                      <div className="d-flex justify-content-between align-items-center pb-2">
                        <h3>Dataset 1</h3>
                        <div className="d-flex gap-3 in-cd">
                          <button onClick={() => reDirect("/dashboard")}>
                            <Image src={icon_grid} alt="" />
                          </button>
                          <button onClick={() => reDirect("/data")}>
                            <Image src={icon_eye} alt="" />
                          </button>
                        </div>
                      </div>
                      <div className="Image-hldr">
                        <Image src={PwC_fl_c} alt="" />
                      </div>
                      <div className="Image-desc">
                        <p>
                          This dataset contains information about several
                          different ...
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="in-mc-sec">
                  {" "}
                  <div
                    className="in-p-sec d-flex flex-wrap"
                    style={{ marginLeft: 20 }}
                  >
                    No dataset found, Please add a dataset
                  </div>
                </div>
              )}
            </section>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <div
                className="alpha-hldr"
                data-bs-backdrop="static"
                data-bs-keyboard="false"
                aria-labelledby="staticBackdropLabel"
                aria-hidden="true"
              >
                <div className="in-pop-datahldr d-flex flex-column pt-4">
                  <div className="in-pop-header d-flex justify-content-between">
                    <h2>Add New Dataset</h2>
                    <button onClick={handleClose}>
                      <Image src={ionic_close} alt="" />
                    </button>
                  </div>
                  <div className="in-pop-body d-flex flex-column h-100">
                    <form className="in-pop-search w-100 d-flex align-items-center">
                      <div className="in-pop-src w-100 mb-4 px-3 d-flex flex-column">
                        <input
                          className="form-control mx-auto"
                          type="search"
                          placeholder="Name of Dataset"
                          aria-label="Search"
                        />
                        <input
                          className="form-control mx-auto"
                          type="search"
                          placeholder="Description"
                          aria-label="Search"
                        />
                      </div>
                    </form>
                    <div className="container-fluid p-0 h-100 align-items-stretch">
                      <h2>Select Source(s)</h2>
                      <div
                        className="row _h-100"
                        style={{ height: "calc(100% - 3rem)" }}
                      >
                        <div className="col-4 in-left-panel">
                          <ul className="in-pop-flink">
                            <li>
                              <a href="#url">All</a>
                            </li>
                            <li>
                              <a href="#url" className="active">
                                Local File Upload
                              </a>
                            </li>
                            <li>
                              <a href="#url">Cloud Storage</a>
                            </li>
                            <li>
                              <a href="#url">Databases</a>
                            </li>
                            <li>
                              <a href="#url">API</a>
                            </li>
                          </ul>
                        </div>

                        <div className="col-8 align-items-stretch in-pop-lb in-right-panel">
                          {!loading ? (
                            <UploadDatasetButton
                              onUpload={handleDatasetChange}
                              resetContext={resetContext}
                            />
                          ) : (
                            <div className="loader-hldr">
                              <div className="loader"></div>
                              <p style={{ marginTop: 10 }}>Please wait ...</p>
                            </div>
                          )}
                        </div>

                        {/* <div className="col-8 align-items-stretch in-pop-lb in-right-panel">
                          <div className="in-pl-hldr d-flex flex-wrap">
                            <dl className="in-pop-plist">
                              <dd>
                                <Image
                                  src={icon_excel}
                                  alt=""
                                  style={{ width: "45%" }}
                                />
                              </dd>
                              <dt>Upload CSV </dt>
                            </dl>
                          </div>
                        </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>
          </>
        )}
      />
    </>
  );
}
