import * as React from "react";
import Chat from "../components/viz/Chat";
import PageLayout from "../components/layout/PageLayout";
export default function manual() {
  return <PageLayout Body={() => <Chat />} />;
}
