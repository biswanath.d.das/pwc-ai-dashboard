import React, { useState } from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
  Paper,
} from "@mui/material";

interface DataItem {
  [key: string]: string;
}

interface DynamicDataTableProps {
  data: DataItem[];
}

const DynamicDataTable: React.FC<DynamicDataTableProps> = ({ data }) => {
  const headers = Object.keys(data[0]);
  const [page, setPage] = useState(0);

  const [rowsPerPage, setRowsPerPage] = useState(8);

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const paginatedData = data.slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );

  return (
    <TableContainer component={Paper} style={{ border: "1px solid #808080a1" }}>
      <Table>
        <TableHead style={{ background: "rgb(0 0 0)" }}>
          <TableRow>
            {headers.map((header) => (
              <TableCell key={header} style={{ color: "white" }}>
                {header}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {paginatedData.map((row, index) => (
            <TableRow key={index}>
              {headers.map((header) => (
                <TableCell key={header}>{row[header]}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        style={{ float: "left", marginTop: 20 }}
      />
    </TableContainer>
  );
};

export default DynamicDataTable;
