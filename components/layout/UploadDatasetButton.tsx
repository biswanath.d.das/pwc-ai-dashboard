import React from "react";
import { Button } from "./Button";
import styles from "../../styles/Components.module.scss";
import { parseData, stringifyData } from "../../utils/parseData";
import { useCoreData } from "../../pages/_app";
import Image from "next/image";
import icon_csv2 from "../../images/icon_upload-big.svg";
interface UploadDatasetButtonProps {
  onUpload: (dataset: string) => void;
  resetContext: any;
  ref?: any;
}

function sanitizeArray(arr: any) {
  const sanitizedArray = arr.map((item: any) => {
    const sanitizedItem: any = {};

    let consecutiveEmptyValues = 0;

    for (const key in item) {
      let value = item[key];

      if (value === "") {
        value = 0;
        consecutiveEmptyValues++;
      } else if (value === null) {
        consecutiveEmptyValues++;
      } else {
        consecutiveEmptyValues = 0;
        if (!isNaN(value)) {
          value = parseFloat(value);
        }
      }

      if (consecutiveEmptyValues <= 4) {
        sanitizedItem[key] = value;
      }
    }

    return sanitizedItem;
  });

  return sanitizedArray;
}

export function UploadDatasetButton(props: UploadDatasetButtonProps) {
  let { coreData, setCoreData } = useCoreData();

  const inputFileRef = React.useRef<HTMLInputElement>(null);
  const handleUploadFileClick = React.useCallback(() => {
    inputFileRef.current?.click?.();
  }, []);
  const [settings, setSettings] = React.useState<any>("");
  const handleDownload = () => {
    const filePath = "data.csv"; // Specify the path to your sample CSV file

    // Create a temporary anchor element
    const link = document.createElement("a");
    link.href = filePath;
    link.download = "sample.csv"; // Specify the desired filename

    // Simulate a click event to trigger the download
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  React.useEffect(() => {
    const getData = localStorage.getItem("analyzer-settings");
    const settingsData = getData && JSON.parse(getData);
    setSettings(settingsData);
  }, []);

  const handleUploadFile = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();

      props.resetContext();
      if (e.target) {
        const inputFile = (e.target as HTMLInputElement).files?.[0];
        const reader = new FileReader();

        reader.onload = function (event) {
          const text = event?.target?.result as string;
          if (text) {
            const data = parseData(text);

            console.log("============>", sanitizeArray(data));

            // eslint-disable-next-line react-hooks/exhaustive-deps

            setCoreData(sanitizeArray(data));

            props.onUpload?.(stringifyData(data));
          }
        };

        if (inputFile) reader.readAsText(inputFile);
      }
    },
    [props.onUpload]
  );

  return (
    <>
      {/* <Button onClick={handleUploadFileClick} style={{ display: "none" }}>
        Upload Data
      </Button> */}

      <div className="in-upl-hldr" onClick={handleUploadFileClick}>
        <Image src={icon_csv2} alt="d" />
        <div className="text-center">
          <p>
            <strong>Click to upload or drag and drop files here</strong>
          </p>
          <p style={{ fontSize: 15 }}>Maximum file size : 200MB</p>
          <p>Supported files : CSV</p>
        </div>
      </div>
      {settings?.mock === "true" && (
        <center>
          <button
            onClick={handleDownload}
            className="in-btn-cmn in-btn-primary"
            style={{ fontSize: 15, marginTop: 40 }}
          >
            Download Sample CSV
          </button>
        </center>
      )}
      <input
        ref={inputFileRef}
        hidden
        type="file"
        onChange={handleUploadFile}
        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
      />
    </>
  );
}
