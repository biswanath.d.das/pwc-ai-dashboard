import Head from "next/head";
import React from "react";
import { styled, useTheme, Theme, CSSObject } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import { ReactJSXElementChildrenAttribute } from "@emotion/react/types/jsx-namespace";
import InsertChartIcon from "@mui/icons-material/InsertChart";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import { useRouter } from "next/router";
import CalculateIcon from "@mui/icons-material/Calculate";
import Image from "next/image";
import Script from "next/script";

import Imagex from "../../images/PwC_fl_wo.svg";

const drawerWidth = 240;

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

type Props = {
  Body: any;
};
export default function MiniDrawer({ Body }: Props) {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const router = useRouter();
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const reDirect = (path: string) => {
    router.push(path);
    // window.location.replace(path);
  };

  const isActiveRoute = (path: any) => {
    return router.pathname === path;
  };

  return (
    <>
      <Script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
      ></Script>
      <Head>
        <title>Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="dashboard" />

        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9"
          crossOrigin="anonymous"
        />

        {/* OG Meta tags */}
        <meta name="og:type" content="website" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <div className="container-fluid d-flex flex-column p-0 h-100">
        <div className="in-search-top d-flex align-items-center justify-content-between px-3">
          <div className="d-flex in-logo">
            <span className="in-logo-hld me-3">
              <Image src={Imagex} alt="" />
            </span>
            <span>VisualSense</span>
          </div>

          <div style={{ padding: 50 }}></div>
        </div>

        <div className="in-body-sec d-flex align-items-stretch justify-content-between h-100">
          <section className="d-flex align-items-stretch h-100 in-left-section in-left-section flex-column gap-5 pt-5">
            <div>
              <ul className="in-ltlist">
                <li className="in-list1" onClick={() => reDirect("/addData")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/addData") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li className="in-list2" onClick={() => reDirect("/dashboard")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/dashboard") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li className="in-list3" onClick={() => reDirect("/report")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/report") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li className="in-list4" onClick={() => reDirect("/doc")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/doc") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li className="in-list-bot" onClick={() => reDirect("/chat")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/chat") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li
                  className="in-list-manual"
                  onClick={() => reDirect("/manual")}
                >
                  <a
                    href="#url"
                    className={isActiveRoute("/manual") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
                <li className="in-list5" onClick={() => reDirect("/settings")}>
                  <a
                    href="#url"
                    className={isActiveRoute("/settings") ? "active" : ""}
                  >
                    Link
                  </a>
                </li>
              </ul>
            </div>
            <div className="in-pImage mt-auto mb-1 mx-auto">
              <Image src={Imagex} alt="" />
            </div>
          </section>

          <Body />
        </div>
      </div>
    </>
  );
}
