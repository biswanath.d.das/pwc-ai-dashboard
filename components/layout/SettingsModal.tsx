import React from "react";
import styles from "../../styles/Components.module.scss";
import { Button } from "./Button";
import { TextInput } from "./TextInput";
import SelectInput from "./SelectInput";
import { GPT_MODEL, MOCK } from "../../openai/constants";
import swal from "sweetalert";

export function SettingsModal(
  props: React.PropsWithChildren<{
    value: {
      apikey: string;
      sampleRows: number;
      model: string;
      mock: string;
      context: string;
    };
    onChange?: (value: {
      apikey: string;
      sampleRows: number;
      model: string;
      mock: string;
      context: string;
    }) => void;
    onCancel?: () => void;
  }>
) {
  const [settings, setSettings] = React.useState(props.value);

  const handleApiKeyChange = React.useCallback((apikey: string) => {
    setSettings((settings) => ({ ...settings, apikey }));
  }, []);

  const handleRowsSampleChange = React.useCallback((sampleRows: number) => {
    setSettings((settings) => ({ ...settings, sampleRows }));
  }, []);

  const handleModelChange = React.useCallback((model: string) => {
    setSettings((settings) => ({ ...settings, model }));
  }, []);

  const handleMockChange = React.useCallback((mock: string) => {
    setSettings((settings) => ({ ...settings, mock }));
  }, []);

  const handleContextKeyChange = React.useCallback((context: string) => {
    setSettings((settings) => ({ ...settings, context }));
  }, []);

  const handleSave = React.useCallback(() => {
    props.onChange?.(settings);
    swal("Success!", "Configration updated!", "success");
  }, [props.onChange, settings]);

  return (
    <div className="container in-setting w-50">
        <div className={styles.settingsContent}>
          <div className="mb-3">
            <TextInput
              value={settings.apikey}
              onChange={handleApiKeyChange}
              label={<>API Key </>}
              type="text"
            />
            </div>
            <div className="mb-3">
            <TextInput
              value={settings.context}
              onChange={handleContextKeyChange}
              label={<>Context </>}
              type="text"
            />
            </div>
            <div className="mb-3">
            <TextInput
              value={settings.sampleRows}
              onChange={handleRowsSampleChange}
              label="Rows to sample"
              type="number"
            />
            </div>
            {/* <SelectInput
              onChange={handleModelChange}
              options={Object.values(GPT_MODEL)}
              title="Model"
              value={settings.model}
            /> */}
            <div className="mb-3">
            <SelectInput
              onChange={handleMockChange}
              options={Object.values(MOCK)}
              title="Mock Use"
              value={settings.mock}
            />
            </div>
          </div>
          <div className={styles.settingsFooter}>
            <button className="in-btn-cmn in-btn-primary" onClick={handleSave}>
              Save
            </button>
          </div>
    </div>
  );
}
