import React from "react";
import { Box, Typography, List, ListItem, ListItemText } from "@mui/material";
import MockSuggestion from "../../openai/Mock/suggestion_MOCL.json";
import MockSuggestionResponse from "../../openai/Mock/suggestion_MOCK_full.json";
import Button from "@mui/material/Button";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import Modal from "@mui/material/Modal";
import { OPEN_AI_COMPLETIONS_DAVINCI_API } from "../../config";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  //   width: 600,
  height: 700,
  overflow: "auto",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

interface HtmlDisplayProps {
  htmlContent: any;
}

const HtmlDisplayComponent: React.FC<HtmlDisplayProps> = ({ htmlContent }) => {
  return <div dangerouslySetInnerHTML={{ __html: htmlContent }} />;
};

const BusinessImprovementList: React.FC<any> = ({ chartData, title }: any) => {
  const [data, setData] = React.useState(MockSuggestion);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const fetchData = async () => {
    setLoading(true);
    const analyzer_settings = localStorage.getItem("analyzer-settings");
    const settings = analyzer_settings && JSON.parse(analyzer_settings);
    const prompt = `Give financial analysis and business improvement suggestions on the basis of bellow Input data with given Context 
            Context:
                ${settings?.context}
            
            Input data:    
                ${JSON.stringify(chartData)}
            
            Instructions:
                1. Give a list of 8 suggestions with a heading and description
                2. give a short description of each point
                3. give some example 
                4. give a predection of how much can me improved 
                5. Give a suggestion sales improvement 
                6. Give financial analysis Description at the top 

            The following topics must be considered:
                - Reply only in valid HTML code with paragraph and heading
                - Description should be contain one example from the data set
            Example reply:
                <div>
                    <p>
                        small analysis.....
                    </p>
                    <h4>Heading 1</h4>
                    <p>Description 1</p>
                    <h4>Heading 2</h4>
                    <p>Description 2</p>
                    ....
                    <p>
                        suggestion sales improvement.....
                    </p>
                </div>
    
    `;

    const config = localStorage.getItem("analyzer-settings");
    const settingData = config && JSON.parse(config);

    if (settingData?.apikey && open) {
      await fetch(OPEN_AI_COMPLETIONS_DAVINCI_API, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${settingData.apikey}`,
        },
        method: "POST",
        body: JSON.stringify({
          max_tokens: 2000,
          model: "text-davinci-003",
          temperature: 1,
          prompt,
        }),
      })
        .then((response) => response.json())
        .then((resp) => {
          //   console.log("====resp=======>", resp.choices?.[0]?.text);
          //   const jsonObject = JSON.parse(resp.choices?.[0]?.text);
          setData(resp.choices?.[0]?.text);
          //   console.log("====formated=======>", jsonObject);
          setLoading(false);
        });
    }
  };

  React.useEffect(() => {
    fetchData();
  }, [open]);

  return (
    <>
      <Button
        size="small"
        variant="outlined"
        color="error"
        onClick={handleOpen}
      >
        <ManageSearchIcon />
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          {loading ? (
            <div className="center">
              <div className="center-button-wrapper">
                <center>
                  <div className="loader"></div>
                  <br />
                  Analyzing data...
                </center>
              </div>
            </div>
          ) : (
            <>
              {/* <Typography variant="h4" gutterBottom>
               
              </Typography> */}

              <HtmlDisplayComponent htmlContent={data} />
            </>
          )}
        </Box>
      </Modal>
    </>
  );
};

export default BusinessImprovementList;
