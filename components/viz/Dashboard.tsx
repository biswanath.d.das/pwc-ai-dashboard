import React, { useRef } from "react";
import html2canvas from "html2canvas";
import { IDashboard, IDataset, IDatasetRecord } from "../../types";
import styles from "../../styles/Components.module.scss";
import { DropdownFilter } from "./DropdownFilter";
import { PerformanceIndicator } from "./PerformanceIndicator";
import { LineChart } from "./LineChart";
import { BarChart } from "./BarChart";
import { PieChart } from "./PieChart";
import { className } from "../../utils/className";
import { TreemapChart } from "./TreemapChart";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import DownloadForOfflineIcon from "@mui/icons-material/DownloadForOffline";
import CloseIcon from "@mui/icons-material/Close";
import GetAppIcon from "@mui/icons-material/GetApp";
import TabData from "./Tab";
import ManualAnalycys from "./ManualAnalycys";
import GenerateDoc from "./GenerateDoc";
import GenerateReport from "./GenerateReport";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import icon_list2 from "../../images/icon-list2.svg";
import Image from "next/image";
import icon_filter from "../../images/icon_filter.svg";
import Suggestions from "./Suggestion";
import DateAnalitics from "./DateAnalitics";

import icon_share from "../../images/icon_share.svg";
import PwC_fl_c from "../../images/PwC_fl_c.png";
import icon_download from "../../images/icon_download.svg";
import icon_refresh from "../../images/icon_refresh.svg";

import icon_grid from "../../images/icon_grid.svg";
import { display } from "html2canvas/dist/types/css/property-descriptors/display";
import LeftArrow from "../../images/icon_lt-chevron.svg";
import RightArrow from "../../images/icon_rt-chevron.svg";
import FilterAltIcon from "@mui/icons-material/FilterAlt";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

export function Dashboard(
  props: React.PropsWithChildren<{
    dashboard: IDashboard;
    data: IDataset;
    onAnalyze: any;
  }>
) {
  const [filters, setFilters] = React.useState<
    Pick<IDatasetRecord, keyof IDatasetRecord>
  >({});

  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(true);

  console.log("filters=======", Object.keys(filters));

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleSidebar = () => {
    setOpen(!open);
  };

  const divRef = useRef(null);
  const handleFilterChange = React.useCallback((filter: string) => {
    return (value: string) => {
      setFilters((filters) => ({ ...filters, [filter]: value }));
    };
  }, []);

  const filteredData = React.useMemo(() => {
    if (Object.keys(filters).length) {
      return Object.keys(filters).reduce((result, key) => {
        if (filters[key])
          return result.filter((row) => row[key] == filters[key]);
        return result;
      }, props.data);
    }
    return props.data;
  }, [filters, props.data]);

  const saveAsPNG = (index: any) => {
    // const divToCapture = divRef.current;
    const divToCapture = document.getElementById(index);

    if (divToCapture) {
      html2canvas(divToCapture).then((canvas) => {
        const link = document.createElement("a");
        link.href = canvas.toDataURL("image/png");
        link.download = "captured_div.png";
        link.click();
      });
    }
  };
  const deleteGraph = (index: any) => {
    // const divToCapture = divRef.current;
    const divToCapture = document.getElementById(index);
    console.log("divToCapture", divToCapture);
    if (divToCapture) {
      divToCapture.remove();
      // divToCapture.style.display = "none";
    }
  };

  localStorage.setItem("data-summary", props.dashboard?.summary?.details);

  console.log("===filter", props.dashboard.filters);

  const filterName = (column: string) => {
    const data: any = props.dashboard.filters.find(
      (val) => val.column === column
    );

    console.log("data", data);
    const updatedText = data?.title.replace("Filter by ", "");
    return updatedText;
  };
  return (
    <>
      <section className="w-100 d-flex h-100 in-center-section flex-column px-4">
        <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-4 in-ct-ht">
          <div>
            <h2 className="mb-1">Dashboard</h2>
          </div>
          <div>
            <button
              className="in-btn-cmn me-2 in-btn-pb"
              onClick={props.onAnalyze}
            >
              <span className="me-2">
                <Image src={icon_refresh} alt="" />
              </span>{" "}
              Re-analyze
            </button>
            <button
              className="in-btn-cmn me-2 in-btn-p50"
              onClick={() => saveAsPNG(`dashboard-section`)}
            >
              <span className="me-2">
                <Image src={icon_download} alt="" />
              </span>{" "}
              Download
            </button>
            {/* <button className="in-btn-cmn me-2 in-btn-p50">
              <span className="me-2">
                <Image src={icon_share} alt="" />
              </span>{" "}
              Share
            </button> */}
          </div>
        </div>

        <>
          <div id="dashboard-section" style={{ padding: 10 }}>
            <div className="d-flex gap-3 px-3 mb-2">
              {props.dashboard.kpis.map((filter, index) => (
                <PerformanceIndicator
                  key={`${filter.title}-${index}`}
                  config={filter}
                  data={filteredData}
                />
              ))}
            </div>
            {/* <div style={{ padding: 0 }} className="px-3">
              <div className={className(styles.chartCard)} ref={divRef}>
                <div className={styles.chartCardTitle}>Summary</div>
                <p style={{ color: "#0808087a" }}>
                  {props.dashboard?.summary?.details}
                </p>
              </div>
            </div> */}
            <div className="in-chart-section">
              {props.dashboard.charts.map((chart, index) => (
                <div
                  key={`${chart.title}-${index}`}
                  className="in-chart-hldr d-flex flex-column"
                  ref={divRef}
                  id={`${chart.title}-${index}`}
                >
                  {chart.chartType === "lineChart" && (
                    <>
                      <div className="d-flex justify-content-between align-items-center in-char-tbtn">
                        <h5 style={{ marginBottom: 20 }}>{chart.title}</h5>

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <GetAppIcon
                            onClick={() => saveAsPNG(`${chart.title}-${index}`)}
                            style={{ cursor: "pointer" }}
                            color="info"
                          />
                          <DeleteOutlineIcon
                            onClick={() =>
                              deleteGraph(`${chart.title}-${index}`)
                            }
                            style={{
                              cursor: "pointer",
                              color: "#009deb",
                              marginLeft: 5,
                            }}
                          />
                        </div>
                      </div>
                      <div>
                        <div style={{ height: "20rem" }}>
                          <TabData
                            config={chart}
                            data={filteredData}
                            type={chart.chartType}
                          />
                        </div>
                      </div>
                    </>
                  )}

                  {chart.chartType === "barChart" && (
                    <>
                      <div className="d-flex justify-content-between align-items-center in-char-tbtn">
                        <h5 style={{ marginBottom: 20 }}>{chart.title}</h5>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div>
                            <GetAppIcon
                              onClick={() =>
                                saveAsPNG(`${chart.title}-${index}`)
                              }
                              style={{ cursor: "pointer" }}
                              color="info"
                            />
                            <DeleteOutlineIcon
                              onClick={() =>
                                deleteGraph(`${chart.title}-${index}`)
                              }
                              style={{
                                cursor: "pointer",
                                color: "#009deb",
                                marginLeft: 5,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div>
                        {" "}
                        <div style={{ height: "20rem" }}>
                          <TabData
                            config={chart}
                            data={filteredData}
                            type={chart.chartType}
                          />
                        </div>
                      </div>
                    </>
                  )}
                  {chart.chartType === "pieChart" && (
                    <>
                      <div className="d-flex justify-content-between align-items-center in-char-tbtn">
                        <h5 style={{ marginBottom: 20 }}>{chart.title}</h5>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div>
                            <GetAppIcon
                              onClick={() =>
                                saveAsPNG(`${chart.title}-${index}`)
                              }
                              style={{ cursor: "pointer" }}
                              color="info"
                            />
                            <DeleteOutlineIcon
                              onClick={() =>
                                deleteGraph(`${chart.title}-${index}`)
                              }
                              style={{
                                cursor: "pointer",
                                color: "#009deb",
                                marginLeft: 5,
                              }}
                            />
                            {/* <Suggestions
                              chartData={filteredData}
                              style={{
                                cursor: "pointer",
                                color: "#009deb",
                                marginLeft: 5,
                              }}
                            />
                            {filteredData && filteredData?.length > 0 && (
                              <DateAnalitics
                                filteredData={filteredData}
                                style={{
                                  cursor: "pointer",
                                  color: "#009deb",
                                  marginLeft: 5,
                                }}
                              />
                            )} */}
                          </div>
                        </div>
                      </div>
                      <div>
                        {" "}
                        <div style={{ height: "20rem" }}>
                          <TabData
                            config={chart}
                            data={filteredData}
                            type={chart.chartType}
                          />
                        </div>
                      </div>
                    </>
                  )}
                  {chart.chartType === "treemapChart" && (
                    <>
                      <div className="d-flex justify-content-between align-items-center in-char-tbtn">
                        <h5 style={{ marginBottom: 20 }}>{chart.title}</h5>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div>
                            <GetAppIcon
                              onClick={() =>
                                saveAsPNG(`${chart.title}-${index}`)
                              }
                              style={{ cursor: "pointer" }}
                              color="info"
                            />
                            <DeleteOutlineIcon
                              onClick={() =>
                                deleteGraph(`${chart.title}-${index}`)
                              }
                              style={{
                                cursor: "pointer",
                                color: "#009deb",
                                marginLeft: 5,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div>
                        <div style={{ height: "20rem" }}>
                          <TreemapChart config={chart} data={filteredData} />
                        </div>
                      </div>
                    </>
                  )}
                </div>
              ))}
            </div>
          </div>
        </>
      </section>
      <div className="in-btn-hiderp" onClick={handleSidebar}>
        {!open ? (
          <FilterAltIcon
            style={{
              margin: 16,
              fontSize: "1.5rem",
              color: "white",
            }}
          />
        ) : (
          <Image
            src={RightArrow}
            alt=""
            style={{
              height: 30,
              margin: 10,
            }}
          />
        )}
        {/* <Image src={icon_refresh} alt="" /> */}
      </div>
      <section className="flex-column align-items-stretch h-100 w-100 in-right-section _pb-3">
        <div className="in-rs-hldr">
          <h2>
            <span>
              Filters <Image src={icon_filter} alt="" />
            </span>
          </h2>

          <div className="px-3 in-lp-filter">
            {props.dashboard.filters.map((filter, index) => (
              <DropdownFilter
                key={`${filter.column}-${index}`}
                config={filter}
                data={props.data}
                onChange={handleFilterChange(filter.column)}
                value={filters[filter.column]}
              />
            ))}
          </div>
        </div>

        <div className="in-rs-hldr">
          <h2>
            <span>
              Dashboard Lens <Image src={icon_grid} alt="" />
            </span>
          </h2>
          <div className="ps-4 in-rs-bc">
            {/* <div>
              <span>Primary</span>
            </div> */}
            {Object.keys(filters)?.map((val, i) => {
              if (filters[val])
                return (
                  <div key={i}>
                    <span className="active">
                      {filterName(val)}:{` `}
                      <b style={{ fontSize: 15 }}>{filters[val]}</b>
                    </span>
                  </div>
                );
            })}

            {/* <div>
              <span>Top 20 Companies</span>
            </div> */}
          </div>
        </div>
      </section>
    </>
  );
}
