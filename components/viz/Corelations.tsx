import React from "react";
import CoRelationGraph from "./sub/CoRelationGraph";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import styles from "../../styles/Components.module.scss";
interface Props {
  correlationCoefficients: number[];
  combinations: string[][];
  iniStateData: any;
}

interface DataRecord {
  [key: string]: number | string;
}

const CorrelationCoefficientsTable: React.FC<Props> = ({
  correlationCoefficients,
  combinations,
  iniStateData,
}) => {
  const coRelationJSon = correlationCoefficients.map((coefficient, index) => ({
    combinations: combinations[index],
    coefficient: coefficient.toFixed(2),
  }));

  return (
    <div style={{ marginTop: 20 }}>
      <div className={styles.chartCardTitle}>Correlation Coefficients</div>

      <TableContainer component={Paper} style={{ marginTop: 20 }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Combination</TableCell>
              <TableCell align="center">Correlation Coefficient</TableCell>
              <TableCell align="center">Visualize</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {correlationCoefficients.map((coefficient, index) => (
              <TableRow
                key={index}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="left">
                  {combinations[index].join(", ")}
                </TableCell>
                <TableCell align="center">{coefficient.toFixed(2)}</TableCell>
                <TableCell align="center">
                  {iniStateData && (
                    <CoRelationGraph
                      iniStateData={iniStateData}
                      graph={combinations[index]}
                    />
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

function findNumericColumnCombinations(data: any): string[][] {
  const numericColumns: string[] = [];

  // Identify numeric columns
  if (data?.length > 0) {
    const sampleObject = data[0];
    for (const key in sampleObject) {
      const value = sampleObject[key];
      if (!isNaN(Number(value))) {
        numericColumns.push(key);
      }
    }
  }

  // Find all combinations of numeric columns
  const columnCombinations: string[][] = [];
  const totalColumns = numericColumns.length;

  for (let i = 1; i <= totalColumns; i++) {
    const combinations = getCombinations(numericColumns, i);
    columnCombinations.push(...combinations);
  }

  return columnCombinations;
}

function getCombinations(arr: string[], k: number): string[][] {
  const result: string[][] = [];
  const current: string[] = [];
  function backtrack(start: number) {
    if (current.length === k) {
      result.push([...current]);
      return;
    }
    for (let i = start; i < arr.length; i++) {
      current.push(arr[i]);
      backtrack(i + 1);
      current.pop();
    }
  }
  backtrack(0);
  return result;
}

interface ChildComponentProps {
  stateData: any;
}

const App: React.FC<ChildComponentProps> = ({ stateData }) => {
  function calculateCorrelationCoefficient(
    data: DataRecord[],
    combinations: string[][]
  ): number[] {
    const correlationCoefficients: number[] = [];

    for (const combination of combinations) {
      const xValues: number[] = [];
      const yValues: number[] = [];

      if (data) {
        for (const record of data) {
          const values = combination.map((column) => Number(record[column]));

          xValues.push(values[0]);
          yValues.push(values[1]);
        }
      }

      // Calculate correlation coefficient (Pearson correlation coefficient)
      const n = xValues.length;
      const sumX = xValues.reduce((acc, val) => acc + val, 0);
      const sumY = yValues.reduce((acc, val) => acc + val, 0);
      const sumXY = xValues.reduce(
        (acc, val, index) => acc + val * yValues[index],
        0
      );
      const sumX2 = xValues.reduce((acc, val) => acc + val * val, 0);
      const sumY2 = yValues.reduce((acc, val) => acc + val * val, 0);

      const numerator = n * sumXY - sumX * sumY;
      const denominator = Math.sqrt(
        (n * sumX2 - sumX * sumX) * (n * sumY2 - sumY * sumY)
      );

      const correlationCoefficient = numerator / denominator;
      correlationCoefficients.push(correlationCoefficient);
    }

    return correlationCoefficients;
  }

  const numericColumnCombinations = findNumericColumnCombinations(stateData);

  const correlationCoefficients = calculateCorrelationCoefficient(
    stateData,
    numericColumnCombinations
  );
  return (
    <>
      <CorrelationCoefficientsTable
        correlationCoefficients={correlationCoefficients}
        combinations={numericColumnCombinations}
        iniStateData={stateData}
      />
    </>
  );
};

export default App;
