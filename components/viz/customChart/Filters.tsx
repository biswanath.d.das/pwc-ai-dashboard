import React from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import styles from "../../../styles/Components.module.scss";

// Assuming all data items have the same keys

const SelectsByKeys: React.FC<any> = ({ data, getFilters }: any) => {
  const [filters, setFilters] = React.useState<{ [key: string]: string }>({});

  const handleSelectChange = (event: any, key: string) => {
    const selectedValue = event.target.value as string;
    const updatedFilters = { ...filters };

    if (selectedValue === "") {
      delete updatedFilters[key];
    } else {
      updatedFilters[key] = selectedValue;
    }
    getFilters(updatedFilters);
    setFilters(updatedFilters);
  };

  // Create an object to store the grouped values for each key
  const groupedValues: { [key: string]: string[] } = {};

  // Group the values for each key
  data.forEach((item: any) => {
    Object.keys(item).forEach((key) => {
      if (!groupedValues[key]) {
        groupedValues[key] = [];
      }
      if (!groupedValues[key].includes(item[key])) {
        groupedValues[key].push(item[key]);
      }
    });
  });

  return (
    <>
      <div
        style={{
          display: "contents",
        }}
      >
        {Object.keys(groupedValues).map((key, index) => (
          <div className={styles.dropdownFilter} key={index}>
            <label style={{ fontSize: 12 }}>{key}</label>
            <select
              value={filters[key] || ""}
              onChange={(event) => handleSelectChange(event, key)}
            >
              <option key={"None"} value="">
                None
              </option>
              {groupedValues[key].map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </select>
          </div>
        ))}
      </div>
    </>
  );
};

export default SelectsByKeys;
