import React, { useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label,
  ResponsiveContainer,
  Brush,
} from "recharts";
import {
  Select,
  MenuItem,
  FormControl,
  FormControlLabel,
  InputLabel,
  Checkbox,
  Card,
} from "@mui/material";
import { palette } from "../../../utils/palette";
import styles from "../../../styles/Components.module.scss";

interface DataItem {
  DATA: string;
  QUANTITYORDERED: string;
  PRICEEACH: string;
  SALES: string;
  ORDERDATE: string;
}

const DynamicBarChart: React.FC<any> = ({ stateData }) => {
  const [xAxis, setXAxis] = useState<string>("DATA");
  const [yAxis1, setYAxis1] = useState<string>("SALES");
  const [yAxis2, setYAxis2] = useState<string>("QUANTITYORDERED");
  const [groupData, setGroupData] = useState<boolean>(true);

  const getXAxisValues = (): string[] => {
    return stateData && Object.keys(stateData[0]);
  };

  function findNumericKeys() {
    const numericKeysSet = new Set();

    // Helper function to check if a value is numeric
    function isNumeric(value: any) {
      return !isNaN(parseFloat(value)) && isFinite(value);
    }

    // Loop through each object in the data array
    for (const obj of stateData) {
      // Loop through each key in the object
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          // Check if the value of the key is numeric
          if (isNumeric(obj[key])) {
            numericKeysSet.add(key);
          }
        }
      }
    }

    // Convert the Set to an array and return
    return Array.from(numericKeysSet);
  }

  const handleXAxisChange = (event: any) => {
    setXAxis(event.target.value as string);
  };

  const handleYAxis1Change = (event: any) => {
    setYAxis1(event.target.value as string);
  };

  const handleYAxis2Change = (event: any) => {
    setYAxis2(event.target.value as string);
  };

  const handleGroupDataChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setGroupData(event.target.checked);
  };

  let chartData: any = stateData;
  if (groupData) {
    const groupedDataMap: Map<string, { [key: string]: number }> = new Map();
    stateData?.forEach((item: any) => {
      const key = item[xAxis];
      const valueY1 = parseFloat(item[yAxis1]);
      const valueY2 = parseFloat(item[yAxis2]);
      if (!isNaN(valueY1) && !isNaN(valueY2)) {
        if (groupedDataMap.has(key)) {
          groupedDataMap.get(key)![yAxis1] += valueY1;
          groupedDataMap.get(key)![yAxis2] += valueY2;
        } else {
          groupedDataMap.set(key, { [yAxis1]: valueY1, [yAxis2]: valueY2 });
        }
      }
    });

    chartData = Array.from(groupedDataMap, ([key, values]) => ({
      [xAxis]: key,
      [yAxis1]: values[yAxis1],
      [yAxis2]: values[yAxis2],
    }));
  }

  const filteredData = chartData.filter(
    (item: any) => item[xAxis] && item[yAxis1] && item[yAxis2]
  );
  return (
    <div style={{ marginTop: 40 }}>
      <div className={styles.chartCardTitle}>Customize Composit Bar Chart</div>
      <div style={{ display: "flex", marginTop: 20 }}>
        <FormControl sx={{ minWidth: 200, marginRight: 5 }}>
          <p style={{ marginBottom: 5, fontSize: 14 }}>
            Select X Axis{" "}
            <b style={{ color: palette[0] }}>( String / Numeric )</b>
          </p>
          <Select value={xAxis} onChange={handleXAxisChange}>
            {getXAxisValues()?.map((value: any) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl sx={{ minWidth: 200, marginRight: 5 }}>
          <p style={{ marginBottom: 5, fontSize: 14 }}>
            Select Y Axis 1 <b style={{ color: palette[0] }}>( Numeric )</b>
          </p>
          <Select value={yAxis1} onChange={handleYAxis1Change}>
            {findNumericKeys()?.map((value: any) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl sx={{ minWidth: 200 }}>
          <p style={{ marginBottom: 5, fontSize: 14 }}>
            Select Y Axis 2 <b style={{ color: palette[0] }}>( Numeric )</b>
          </p>
          <Select value={yAxis2} onChange={handleYAxis2Change}>
            {findNumericKeys()?.map((value: any) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>

      <div>
        <FormControlLabel
          control={
            <Checkbox checked={groupData} onChange={handleGroupDataChange} />
          }
          label="Group Data of X-axis and SUM coresponding Y-axis values"
        />
      </div>
      <Card>
        <div style={{ height: "400px", marginTop: "20px" }}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart width={600} height={400} data={filteredData}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey={xAxis}>
                <Label
                  value={xAxis}
                  position="insideBottom"
                  offset={-10}
                  fontSize={14}
                  fill={palette[0]}
                />
              </XAxis>
              <YAxis orientation="left">
                <Label
                  value={yAxis1}
                  position="insideLeft"
                  angle={-90}
                  offset={10}
                  fontSize={12}
                  fill={palette[0]}
                />
              </YAxis>
              <YAxis yAxisId="yAxis2" orientation="right">
                <Label
                  value={yAxis2}
                  position="insideLeft"
                  angle={-90}
                  offset={10}
                  fontSize={12}
                  fill={palette[0]}
                />
              </YAxis>

              <Tooltip />
              <Legend />
              <Bar dataKey={yAxis1} fill={palette[1]} maxBarSize={50} />
              <Bar
                dataKey={yAxis2}
                fill={palette[3]}
                yAxisId="yAxis2"
                maxBarSize={50}
              />
              <Brush dataKey="name" height={30} stroke="#8884d8" />
            </BarChart>
          </ResponsiveContainer>
        </div>
      </Card>
    </div>
  );
};

export default DynamicBarChart;
