import React from "react";
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer,
} from "recharts";
import { IChart, IDataset } from "../../../types";
import { parseFunc } from "../../../utils/parseFunc";
import { formatNumber } from "../../../utils/numberFormatter";

const data = [
  {
    subject: "Math",
    A: 120,
    B: 110,
    fullMark: 150,
  },
  {
    subject: "Chinese",
    A: 98,
    B: 130,
    fullMark: 150,
  },
  {
    subject: "English",
    A: 86,
    B: 130,
    fullMark: 150,
  },
  {
    subject: "Geography",
    A: 99,
    B: 100,
    fullMark: 150,
  },
  {
    subject: "Physics",
    A: 85,
    B: 90,
    fullMark: 150,
  },
  {
    subject: "History",
    A: 65,
    B: 85,
    fullMark: 150,
  },
];

export function SimpleRadar({ filteredData, xAxis, yAxis }: any) {
  console.log("==filteredData=>", filteredData, xAxis, yAxis);
  return (
    <div style={{ height: "400px", marginTop: "20px" }}>
      <ResponsiveContainer width="100%" height="100%">
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={filteredData}>
          <PolarGrid />
          <PolarAngleAxis dataKey={xAxis} />
          <PolarRadiusAxis />
          <Radar
            name="Mike"
            dataKey={yAxis}
            stroke="#8884d8"
            fill="#8884d8"
            fillOpacity={0.6}
          />
        </RadarChart>
      </ResponsiveContainer>
    </div>
  );
}

export default SimpleRadar;
