import React from "react";
import { Typography, Card, CardContent } from "@mui/material";
import styles from "../../../styles/Components.module.scss";
import { formatNumber } from "../../../utils/numberFormatter";
// Assuming the data array and keyToDisplay are provided as props to this component
const DataCards = ({ data, keyToDisplay, filters }: any) => {
  // Helper function to convert a value to a number, or return 0 if not a number
  const convertToNumber = (value: any) => {
    const num = parseFloat(value);
    return isNaN(num) ? 0 : num;
  };

  const filteredData = filters
    ? data.filter((item: any) =>
        Object.keys(filters).every((key) => item[key] === filters[key])
      )
    : data;

  // Dynamically sum all corresponding values for the specified key
  const totalValue = filteredData.reduce(
    (acc: any, curr: any) => acc + convertToNumber(curr[keyToDisplay]),
    0
  );

  // Display the data in a Material-UI card
  return (
    <div
      className={styles.performanceIndicator}
      style={{ display: totalValue === 0 ? "none" : "" }}
    >
      {totalValue === 0 ? (
        "Not"
      ) : (
        <>
          <div className="in-tbox">
            <h3 style={{ fontSize: 16, height: 100 }}>
              {keyToDisplay.replace("Average", "Avg.")}
            </h3>
            <div>{formatNumber(totalValue)}</div>
            <span style={{ fontSize: 10 }}>
              {parseFloat(totalValue).toFixed(1)}
            </span>
          </div>
        </>
      )}
    </div>
  );
};

export default DataCards;
