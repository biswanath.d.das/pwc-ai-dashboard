import React, { useState } from "react";
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Checkbox,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Card,
} from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import FunctionsIcon from "@mui/icons-material/Functions";
import CalculateIcon from "@mui/icons-material/Calculate";
import AnalyticsIcon from "@mui/icons-material/Analytics";
import styles from "../../../styles/Components.module.scss";
import { palette } from "../../../utils/palette";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import BarChartData from "./Bar";
import Radar from "./Radar";
import PieChart from "./PieChart";

import Box from "@mui/material/Box";

import Grid from "@mui/material/Grid";

const DynamicBarChart: React.FC<any> = ({ data }) => {
  const [xAxis, setXAxis] = useState<string>("DATA");
  const [yAxis, setYAxis] = useState<string>("SALES");
  const [groupData, setGroupData] = useState<boolean>(true);

  const getXAxisValues = (): string[] => {
    return Object.keys(data[0]);
  };

  function findNumericKeys() {
    const numericKeysSet = new Set();

    // Helper function to check if a value is numeric
    function isNumeric(value: any) {
      return !isNaN(parseFloat(value)) && isFinite(value);
    }

    // Loop through each object in the data array
    for (const obj of data) {
      // Loop through each key in the object
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          // Check if the value of the key is numeric
          if (isNumeric(obj[key])) {
            numericKeysSet.add(key);
          }
        }
      }
    }

    // Convert the Set to an array and return
    return Array.from(numericKeysSet);
  }

  const handleXAxisChange = (event: any) => {
    setXAxis(event.target.value as string);
  };

  const handleYAxisChange = (event: any) => {
    setYAxis(event.target.value as string);
  };

  const handleGroupDataChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setGroupData(event.target.checked);
  };

  let chartData = data;
  if (groupData) {
    const groupedData: { [key: string]: number } = {};
    data.forEach((item: any) => {
      const key = item[xAxis];
      const value = parseFloat(item[yAxis]);
      if (!isNaN(value)) {
        groupedData[key] = (groupedData[key] || 0) + value;
      }
    });

    chartData = Object.keys(groupedData).map((key) => ({
      [xAxis]: key,
      [yAxis]: groupedData[key],
    }));
  }

  const filteredData = chartData.filter(
    (item: any) => item[xAxis] && item[yAxis]
  );

  const headers =
    filteredData && filteredData?.length > 0 && Object.keys(filteredData[0]);

  const calculateStatistics = (data: any, columnKey: any) => {
    // Extract the 'y' values from the data based on the dynamic columnKey
    const yValues = filteredData.map((entry: any) => entry[columnKey]);

    const total = yValues.reduce((sum: any, value: any) => sum + value, 0);

    // Calculate the mean
    const mean =
      yValues.reduce((sum: any, value: any) => sum + value, 0) / yValues.length;

    // Calculate the median
    const sortedYValues = [...yValues].sort((a, b) => a - b);
    const middleIndex = Math.floor(sortedYValues.length / 2);
    const median =
      sortedYValues.length % 2 === 0
        ? (sortedYValues[middleIndex - 1] + sortedYValues[middleIndex]) / 2
        : sortedYValues[middleIndex];

    // Calculate the max and min
    const max = Math.max(...yValues);
    const min = Math.min(...yValues);

    return { total, mean, median, max, min };
  };

  const X_statistics = calculateStatistics(data, headers[0]);
  const Y_statistics = calculateStatistics(data, headers[1]);

  return (
    <div>
      <div style={{ display: "flex", marginTop: 20 }}>
        <FormControl sx={{ minWidth: 200, marginRight: 5 }}>
          <p style={{ marginBottom: 5, fontSize: 14 }}>
            Select X Axis{" "}
            <b style={{ color: palette[0] }}>( String / Numeric )</b>
          </p>

          <Select value={xAxis} onChange={handleXAxisChange}>
            {getXAxisValues().map((value) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl sx={{ minWidth: 200 }}>
          <p style={{ marginBottom: 5, fontSize: 14 }}>
            Select Y Axis <b style={{ color: palette[0] }}>( Numeric )</b>
          </p>

          <Select value={yAxis} onChange={handleYAxisChange}>
            {findNumericKeys().map((value: any) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <br />
      <div>
        <FormControlLabel
          control={
            <Checkbox checked={groupData} onChange={handleGroupDataChange} />
          }
          label="Group Data of X-axis and SUM coresponding Y-axis values"
        />
      </div>

      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Card
            style={{
              width: "100%",
              // maxWidth: 360,
              padding: 10,
              display: "flex",
              justifyContent: "space-around",
            }}
          >
            <ListItem
              style={{
                border: `1px solid ${palette[1]}`,
                borderRadius: 7,
                margin: 10,
              }}
            >
              <ListItemAvatar>
                <Avatar style={{ background: palette[1] }}>
                  <AnalyticsIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Total" secondary={Y_statistics?.total} />
            </ListItem>
            <ListItem
              style={{
                border: `1px solid ${palette[1]}`,
                borderRadius: 7,
                margin: 10,
              }}
            >
              <ListItemAvatar>
                <Avatar style={{ background: palette[1] }}>
                  <FunctionsIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Mean (μ)" secondary={Y_statistics?.mean} />
            </ListItem>
            <ListItem
              style={{
                border: `1px solid ${palette[1]}`,
                borderRadius: 7,
                margin: 10,
              }}
            >
              <ListItemAvatar>
                <Avatar style={{ background: palette[1] }}>
                  <CalculateIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Median" secondary={Y_statistics?.median} />
            </ListItem>

            <ListItem
              style={{
                border: `1px solid ${palette[1]}`,
                borderRadius: 7,
                margin: 10,
              }}
            >
              <ListItemAvatar>
                <Avatar style={{ background: palette[1] }}>
                  <BeachAccessIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Maximum Value"
                secondary={Y_statistics?.max}
              />
            </ListItem>
            <ListItem
              style={{
                border: `1px solid ${palette[1]}`,
                borderRadius: 7,
                margin: 10,
              }}
            >
              <ListItemAvatar>
                <Avatar style={{ background: palette[1] }}>
                  <BeachAccessIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Minimum Value"
                secondary={Y_statistics?.min}
              />
            </ListItem>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card>
            <BarChartData
              filteredData={filteredData}
              xAxis={xAxis}
              yAxis={yAxis}
              Y_statistics={Y_statistics}
            />
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card style={{ height: 500 }}>
            <Radar filteredData={filteredData} xAxis={xAxis} yAxis={yAxis} />
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card style={{ height: 500 }}>
            <PieChart filteredData={filteredData} xAxis={xAxis} yAxis={yAxis} />
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card style={{ height: 500 }}>
            {filteredData && filteredData?.length > 0 && (
              <div style={{ overflowY: "auto", height: "30rem" }}>
                <TableContainer component={Paper}>
                  <Table aria-label="dynamic table">
                    <TableHead style={{ background: "#009deb" }}>
                      <TableRow>
                        {/* Generating table headers from the dynamic keys */}
                        {headers.map((header: any) => (
                          <TableCell key={header} style={{ color: "white" }}>
                            {header}
                          </TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {filteredData.map((row: any, index: number) => (
                        <TableRow key={index}>
                          {/* Generating table cells for each data entry */}
                          {headers.map((header: any) => (
                            <TableCell key={header}>{row[header]}</TableCell>
                          ))}
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
            )}
          </Card>
        </Grid>
      </Grid>

      <br />
    </div>
  );
};

const App: React.FC<any> = ({ stateData }) => {
  return (
    <div>
      <div className={styles.chartCardTitle}>Customize Bar Chart</div>
      {stateData && <DynamicBarChart data={stateData} />}
    </div>
  );
};

export default App;
