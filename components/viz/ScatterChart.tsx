import React from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import { IChart, IDataset } from "../../types";
import { parseFunc } from "../../utils/parseFunc";
import { formatNumber } from "../../utils/numberFormatter";

export function ScatterChartDOM(
  props: React.PropsWithChildren<{
    config: IChart;
    data: IDataset;
    chartSelect: any;
  }>
) {
  const myGroupingFunction = React.useMemo(() => {
    return parseFunc(props.config.javascriptFunction, (data: IDataset) => data);
  }, [props.config]);
  const data = React.useMemo(() => {
    if (typeof myGroupingFunction === "function")
      return myGroupingFunction(props.data);
    return null;
  }, [myGroupingFunction, props.config, props.data]);
  if (!data) return null;
  props?.chartSelect(data?.length);
  return (
    <ResponsiveContainer width="100%" height="100%">
      <ScatterChart>
        <CartesianGrid />
        <XAxis stroke="var(--textColor)" dataKey={"x"} />
        <YAxis
          stroke="var(--textColor)"
          dataKey={"y"}
          tickFormatter={formatNumber}
        />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Scatter width={500} height={400} data={data} fill="#8884d8" />
      </ScatterChart>
    </ResponsiveContainer>
  );
}
export default ScatterChartDOM;
