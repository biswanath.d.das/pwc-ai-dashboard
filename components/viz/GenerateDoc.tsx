import React from "react";
import {
  Box,
  Typography,
  FormLabel,
  FormControl,
  RadioGroup,
  Button,
  Card,
  Radio,
} from "@mui/material";
import MockSuggestion from "../../openai/Mock/suggestion_MOCL.json";
import MockSummary from "../../openai/Mock/Summary.json";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { useChatData } from "../../pages/_app";
import styles from "../../styles/Components.module.scss";
import { OPEN_AI_COMPLETIONS_API } from "../../config";
import swal from "sweetalert";
import { filterContextData } from "../../utils/smartFilter";
import Image from "next/image";
import icon_refresh from "../../images/icon_refresh.svg";
interface HtmlDisplayProps {
  htmlContent: any;
}

export function getDataFromWindow() {
  return window; // Replace with your actual code that uses the window object
}

const HtmlDisplayComponent: React.FC<HtmlDisplayProps> = ({ htmlContent }) => {
  return <div dangerouslySetInnerHTML={{ __html: htmlContent }} />;
};

const BusinessImprovementList: React.FC<any> = () => {
  const [data, setData] = React.useState<any>(null);
  const [docType, SetDocType] = React.useState<any>(null);
  const [loading, setLoading] = React.useState(false);
  const [settings, setSettings] = React.useState<any>(null);
  const { chatData } = useChatData();

  const [localStorage, setLocalStorage] = React.useState<any>(null);

  React.useEffect(() => {
    // Dynamically import the code that uses the window object
    import("./clientCode").then((module) => {
      // Call the function that uses the window object
      const data: any = module.getDataFromWindow();
      setLocalStorage(data);
    });

    const analyzer_settings = localStorage.getItem("analyzer-settings");
    const settings = analyzer_settings && JSON.parse(analyzer_settings);
    setSettings(settings);
  }, []);

  const fetchData = async () => {
    localStorage.removeItem("summary-data");
    setLoading(true);

    const prompt = `Generat a professional ${docType} on the basis of bellow Context dataset 

            Context:
            \"\"\"
            ${filterContextData(chatData)}
            \"\"\"
           
            The following topics must be considered:
              - Reply only in valid HTML code with the answer
              - Make it as big as possible as permit the token limit
              - Do it in discriptive manner
              - Use diffrent grid view 
              - Represent in rows and columns
              - Give a detailed texts in easy to understand english.
              - Reply only in valid HTML code with paragraph and heading
              - Description should be contain one example from the data set
              - Heading must ne with h4 HTML tag
              - Put some relivent color from '#009deb', tables and graphics on the html body
              - Make the html more attractive
              - Do not include any chart and images
              - Make the HTML in diffrent columns and rows where applicable.
              - Use box with borders to make it attractive.
            
                
            Example reply:
                <div>
                    Texts...
                </div>
    `;

    if (settings && settings?.apikey && settings?.mock !== "true") {
      await fetch(OPEN_AI_COMPLETIONS_API, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${settings.apikey}`,
        },
        method: "POST",
        body: JSON.stringify({
          model: "gpt-3.5-turbo",
          temperature: 0.5,
          messages: [
            { role: "system", content: "You answer questions." },
            { role: "user", content: prompt },
          ],
        }),
        // body: JSON.stringify({
        //   max_tokens: 2000,
        //   model: "text-davinci-003",
        //   temperature: 1,
        //   prompt,
        // }),
      })
        .then((response) => response.json())
        .then((resp) => {
          setLoading(false);
          if (resp?.error) {
            swal("info!", resp?.error.message, "info").then((value) =>
              setLoading(false)
            );
            return;
          }
          console.log(
            "=====resp.choices?.[0]?.message?.content==>",
            resp.choices?.[0]?.message?.content
          );
          setData(resp.choices?.[0]?.message?.content);
          localStorage.setItem(
            "summary-data",
            JSON.stringify(resp.choices?.[0]?.message?.content)
          );
        })
        .catch((resp) => {
          swal("info!", resp?.error.message, "info").then((value) =>
            setLoading(false)
          );
        });
    } else {
      setData(MockSummary?.choices?.[0]?.message?.content);
      localStorage.setItem(
        "summary-data",
        JSON.stringify(MockSummary?.choices?.[0]?.message?.content)
      );
      setLoading(false);
    }
  };

  const oldData = () => {
    const summaryRaw = localStorage.getItem("summary-data");
    if (summaryRaw) {
      const summary = JSON?.parse(summaryRaw);
      setData(summary);
    }
  };

  React.useEffect(() => {
    oldData();
  }, []);

  return (
    <>
      <div className="w-100 px-3 in-center-section">
        <div className="w-100 d-flex justify-content-between align-items-center pt-4 mb-4 in-ct-ht">
          <div>
            <h2 className="mb-1">Generate Document</h2>
          </div>
        </div>
        <div style={{ width: "100%" }}>
          <Card
            style={{
              padding: 30,
              display: "flex",
              justifyContent: "space-between",
              margin: 30,
            }}
          >
            <FormControl>
              <FormLabel id="demo-radio-buttons-group-label">
                Document Type
              </FormLabel>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue=""
                onChange={(e) => SetDocType(e.target.value)}
                name="radio-buttons-group"
                row
              >
                <FormControlLabel
                  value="Article"
                  control={<Radio />}
                  label="Article"
                />
                <FormControlLabel
                  value="Corporate article"
                  control={<Radio />}
                  label="Corporate article"
                />
                <FormControlLabel
                  value="Economic Bulletin"
                  control={<Radio />}
                  label="Economic Bulletin"
                />
              </RadioGroup>
            </FormControl>

            <button
              className="in-btn-cmn me-2 in-btn-pb"
              onClick={() => fetchData()}
              style={{ marginTop: 20 }}
            >
              <span className="me-2">
                <Image src={icon_refresh} alt="" />
              </span>{" "}
              Generate
            </button>
          </Card>
          {loading ? (
            <div className="center">
              <div className="center-button-wrapper">
                <center>
                  <div className="loader"></div>
                  <br />
                  Generating document...
                </center>
              </div>
            </div>
          ) : (
            <>
              <Card style={{ padding: 30, margin: 30 }}>
                <HtmlDisplayComponent htmlContent={data} />
              </Card>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default BusinessImprovementList;
