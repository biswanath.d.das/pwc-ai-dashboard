import React from "react";
import { IDataset, IKPI } from "../../types";
import styles from "../../styles/Components.module.scss";
import { parseFunc } from "../../utils/parseFunc";
import { ErrorBoundary } from "../layout/ErrorBoundary";
import { formatNumber } from "../../utils/numberFormatter";
import { useChatData } from "../../pages/_app";

export function PerformanceIndicator(
  props: React.PropsWithChildren<{
    config: IKPI;
    data: IDataset;
  }>
) {
  const { chatData } = useChatData();
  const myEvalFunction = React.useMemo(() => {
    return parseFunc(props.config.javascriptFunction, (data: IDataset) => 0);
  }, [props.config]);

  const value = React.useMemo(() => {
    const val = myEvalFunction(props.data);
    if (typeof val === "number") return formatNumber(val);
    return val;
  }, [myEvalFunction, props.data]);

  React.useEffect(() => {
    const val = {
      title: props.config?.title,
      data: value,
    };

    const ifExist = chatData?.find(
      (val: any) => val?.title === props.config?.title
    );

    if (!ifExist) {
      chatData.push(val);
    }
  }, [value]);

  const valNumber: any = myEvalFunction(props.data);
  return (
    <ErrorBoundary>
      <div className="in-tbox">
        <h3>
          {props.config.title.replace("Average", "Avg.")}
        </h3>
        <div>{value}</div>
        <span style={{ fontSize: 10 }}>
          {valNumber && parseFloat(valNumber).toFixed(1)}
        </span>
      </div>
    </ErrorBoundary>
  );
}
