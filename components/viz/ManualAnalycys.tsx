import React, { useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  AreaChart,
  Area,
  ReferenceLine,
  ComposedChart,
  Bar,
  BarChart,
} from "recharts";
import styles from "../../styles/Components.module.scss";
import { palette } from "../../utils/palette";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import Alert from "@mui/material/Alert";
import FunctionsIcon from "@mui/icons-material/Functions";
import CalculateIcon from "@mui/icons-material/Calculate";
import AnalyticsIcon from "@mui/icons-material/Analytics";
import Corelations from "./Corelations";
import { MenuItem, Select, FormControl, InputLabel, Card } from "@mui/material";
import { parseData, stringifyData } from "../../utils/parseData";
import CustomizeBarChart from "./customChart/CustomizeChart";
import CompBarChart from "./customChart/CompBarChart";
import DataCard from "./customChart/DataCard";
import Filters from "./customChart/Filters";
import { className } from "../../utils/className";
import { useCoreData } from "../../pages/_app";
function convertData(inputData: any, fieldName: any): any {
  return inputData.map((item: any) => ({
    [fieldName]: item[fieldName],
  }));
}
// Example usage:

function convertDataToIntegers(
  data: number[] | string[],
  propertyName: string
): number[] {
  return data
    .map((item: any) => parseInt(item[propertyName], 10))
    .sort((a: number, b: number) => a - b);
}

const calculateNormalDistribution = (data: number[]) => {
  const mean = data.reduce((sum, value) => sum + value, 0) / data.length;
  const variance =
    data.reduce((sum, value) => sum + Math.pow(value - mean, 2), 0) /
    data.length;
  const stdDeviation = Math.sqrt(variance);

  const coefficient = 1 / (stdDeviation * Math.sqrt(2 * Math.PI));

  const totalArea = data.reduce((sum, value) => {
    const exponent = -0.5 * Math.pow((value - mean) / stdDeviation, 2);
    const normalDistribution = coefficient * Math.exp(exponent);
    return sum + normalDistribution;
  }, 0);

  return data.map((value) => {
    const exponent = -0.5 * Math.pow((value - mean) / stdDeviation, 2);
    const normalDistribution = coefficient * Math.exp(exponent);
    const areaPercentage = (normalDistribution / totalArea) * 100;
    return { value, normalDistribution, areaPercentage };
  });
};

const calculateStatistics = (data: number[]) => {
  const mean = data.reduce((sum, value) => sum + value, 0) / data.length;
  const sortedData = [...data].sort((a, b) => a - b);
  const middleIndex = Math.floor(sortedData.length / 2);
  const median =
    sortedData.length % 2 === 0
      ? (sortedData[middleIndex - 1] + sortedData[middleIndex]) / 2
      : sortedData[middleIndex];

  const variance =
    data.reduce((sum, value) => sum + Math.pow(value - mean, 2), 0) /
    data.length;
  const stdDeviation = Math.sqrt(variance);

  return {
    mean,
    median,
    stdDeviation,
    max: Math.max(...data),
    min: Math.min(...data),
  };
};
interface Props {
  specifiedColumn?: string | null;
}
const App: React.FC<Props> = ({ specifiedColumn = null }: any) => {
  let { coreData } = useCoreData();
  const [selectedField, setSelectedField] = useState<any>(specifiedColumn);
  const [stateData, setStateData] = useState<any>(null);
  const [filters, setFilters] = React.useState<any>(null);
  const rawData: any = stateData && convertData(stateData, selectedField);

  let notNumaric;
  function removeNullFromArray(arr: number[]) {
    const filterData = arr.filter(
      (item) => typeof item === "number" && !isNaN(item)
    );

    if (filterData && filterData.length === 0) {
      notNumaric = true;
    } else {
      notNumaric = false;
    }

    return filterData;
  }

  const data =
    stateData &&
    removeNullFromArray(convertDataToIntegers(rawData, selectedField));
  const normalData = stateData && calculateNormalDistribution(data);
  const statistics = stateData && calculateStatistics(data);

  React.useEffect(() => {
    if (coreData) {
      setStateData(coreData);
    }
  }, []);

  // Step 1: Calculate the frequency distribution of the data
  const frequencyDistribution: { [quantityOrdered: string]: number } = {};
  data?.forEach((item: any) => {
    const quantityOrdered = item;
    frequencyDistribution[quantityOrdered] =
      (frequencyDistribution[quantityOrdered] || 0) + 1;
  });

  // Step 2: Convert the frequency distribution to an array of objects for Recharts
  const chartData = Object.entries(frequencyDistribution).map(
    ([quantityOrdered, frequency]) => ({
      quantityOrdered: parseInt(quantityOrdered, 10),
      frequency,
    })
  );

  const keys = stateData && Object.keys(stateData[0]);

  const getFilters = (data: any) => {
    setFilters(data);
  };

  const filteredData = filters
    ? stateData.filter((item: any) =>
        Object.keys(filters).every((key) => item[key] === filters[key])
      )
    : stateData;

  return (
    <>
      <section className="w-100 d-flex h-100 in-center-section flex-column p-4">
        <div className="d-flex gap-3 px-3 mb-2 manual-dashboard-head-card in-manual-thld">
          {stateData &&
            keys?.map((key: any, index: number) => (
              <DataCard
                data={stateData}
                keyToDisplay={key}
                key={index}
                filters={filters}
              />
            ))}
        </div>
        <div className="d-flex gap-3 px-3 py-4 manual-dashboard-head-card in-manual-filter-hld">
          {stateData && <Filters data={stateData} getFilters={getFilters} />}
        </div>

        <div
          // className={className(styles.chartCard)}
          style={{ height: "100%", width: "90vw" }}
          >
          {filteredData && <CustomizeBarChart stateData={filteredData} />}
          {filteredData && <CompBarChart stateData={filteredData} />}

          <br />
          {!specifiedColumn && (
            <>
              <div className={styles.chartCardTitle}>Select a Column</div>
              <FormControl fullWidth>
                <Select
                  onChange={(e) => setSelectedField(e.target.value)}
                  value={selectedField}
                >
                  {keys?.map((key: any) => (
                    <MenuItem key={key} value={key}>
                      {key}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </>
          )}

          <br />
          {selectedField && notNumaric && (
            <Alert severity="error" style={{ marginTop: 20 }}>
              {selectedField} is not numaric
            </Alert>
          )}
          <br />
          {selectedField && !notNumaric && (
            <>
              <Card style={{ padding: 10 }}>
                <div style={{ display: "flex", marginTop: 20 }}>
                  <div style={{ width: "70%", height: 500 }}>
                    <div className={styles.chartCardTitle}>
                      Normal Distribution
                    </div>
                    <ResponsiveContainer>
                      <ComposedChart
                        data={normalData}
                        margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
                      >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis
                          dataKey="value"
                          type="number"
                          domain={["dataMin", "dataMax"]}
                        />
                        <YAxis />
                        <Tooltip
                          formatter={(value: number, name: string) =>
                            name === "areaPercentage"
                              ? `${value.toFixed(2)}%`
                              : value
                          }
                        />
                        <Legend />
                        <Line
                          type="monotone"
                          dataKey="normalDistribution"
                          stroke={palette[0]}
                          activeDot={{ r: 8 }}
                          isAnimationActive={false}
                        />
                        <ReferenceLine
                          x={statistics?.mean}
                          stroke="red"
                          strokeDasharray="3 3"
                          label={{
                            position: "insideBottom",
                            fill: "black",
                            fontSize: 14,
                            value: "Mean (μ) [" + statistics?.mean + "]",
                          }}
                        />
                        <ReferenceLine
                          x={statistics?.median}
                          stroke="red"
                          strokeDasharray="3 3"
                          label={{
                            position: "top",
                            fill: "black",
                            fontSize: 14,
                            value: "Median (" + statistics?.median + ")",
                          }}
                        />
                      </ComposedChart>
                    </ResponsiveContainer>
                  </div>

                  <div style={{ marginTop: "30px" }}>
                    <div className={styles.chartCardTitle}>
                      Statistical Parameters:
                    </div>
                    <List
                      sx={{
                        width: "100%",
                        maxWidth: 360,
                        bgcolor: "background.paper",
                      }}
                    >
                      <ListItem
                        style={{
                          border: `1px solid ${palette[0]}`,
                          borderRadius: 7,
                        }}
                      >
                        <ListItemAvatar>
                          <Avatar style={{ background: palette[0] }}>
                            <FunctionsIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary="Mean (μ)"
                          secondary={statistics?.mean}
                        />
                      </ListItem>
                      <ListItem
                        style={{
                          border: `1px solid ${palette[0]}`,
                          borderRadius: 7,
                          marginTop: 10,
                        }}
                      >
                        <ListItemAvatar>
                          <Avatar style={{ background: palette[0] }}>
                            <CalculateIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary="Median"
                          secondary={statistics?.median}
                        />
                      </ListItem>
                      <ListItem
                        style={{
                          border: `1px solid ${palette[0]}`,
                          borderRadius: 7,
                          marginTop: 10,
                        }}
                      >
                        <ListItemAvatar>
                          <Avatar style={{ background: palette[0] }}>
                            <AnalyticsIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary="Standard Deviation (σ)"
                          secondary={statistics?.stdDeviation}
                        />
                      </ListItem>
                      <ListItem
                        style={{
                          border: `1px solid ${palette[0]}`,
                          borderRadius: 7,
                          marginTop: 10,
                        }}
                      >
                        <ListItemAvatar>
                          <Avatar style={{ background: palette[0] }}>
                            <BeachAccessIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary="Maximum Value"
                          secondary={statistics?.max}
                        />
                      </ListItem>
                      <ListItem
                        style={{
                          border: `1px solid ${palette[0]}`,
                          borderRadius: 7,
                          marginTop: 10,
                        }}
                      >
                        <ListItemAvatar>
                          <Avatar style={{ background: palette[0] }}>
                            <BeachAccessIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary="Minimum Value"
                          secondary={statistics?.min}
                        />
                      </ListItem>
                    </List>
                  </div>
                </div>
              </Card>
              <br />
              <Card style={{ padding: 10 }}>
                <div style={{ width: "100%", height: 500, marginTop: "3rem" }}>
                  <div className={styles.chartCardTitle}>
                    Frequency Distribution
                  </div>
                  <ResponsiveContainer width="100%" height={400}>
                    <BarChart data={chartData}>
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis
                        dataKey="quantityOrdered"
                        type="number"
                        label={{
                          value: "Quantity Ordered",
                          position: "insideBottom",
                          offset: -5,
                        }}
                      />
                      <YAxis
                        label={{
                          value: "Frequency",
                          angle: -90,
                          position: "insideLeft",
                        }}
                      />
                      <Tooltip />
                      <Bar dataKey="frequency" fill={palette[0]} />
                    </BarChart>
                  </ResponsiveContainer>
                </div>
              </Card>
            </>
          )}
        </div>
        {/* {!specifiedColumn && stateData && <Corelations stateData={stateData} />} */}
      </section>
    </>
  );
};

export default App;
