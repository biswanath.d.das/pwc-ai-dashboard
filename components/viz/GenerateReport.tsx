import React from "react";
import {
  Typography,
  FormLabel,
  FormControl,
  RadioGroup,
  Button,
  Card,
  Radio,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import MockSuggestion from "../../openai/Mock/suggestion_MOCL.json";
import MockSummary from "../../openai/Mock/report.json";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { useReportData, useChatData } from "../../pages/_app";
import styles from "../../styles/Components.module.scss";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
} from "recharts";
import html2canvas from "html2canvas";
import { formatNumber } from "../../utils/numberFormatter";
import Img1 from "../../Asset/1.png";
import Head2 from "../../Asset/h2.jpg";
import Head3 from "../../Asset/h3.jpg";
import Img2 from "../../Asset/2.png";
import F3 from "../../Asset/f3.jpg";
import swal from "sweetalert";
import Image from "next/image";
import { filterContextData } from "../../utils/smartFilter";
import { OPEN_AI_COMPLETIONS_API } from "../../config";
import icon_refresh from "../../images/icon_refresh.svg";

const BusinessImprovementList: React.FC<any> = () => {
  const [data, setData] = React.useState<any>(null);

  const [docType, SetDocType] = React.useState<any>(null);
  const [loading, setLoading] = React.useState(false);
  const [editable, setEditable] = React.useState(false);
  const [reportedData, setReportedData] = React.useState([]);
  const { reportData } = useReportData();
  const [headImage, setHeadImage] = React.useState<any>(Head2);
  const [settings, setSettings] = React.useState<any>(null);

  React.useEffect(() => {
    const analyzer_settings = localStorage.getItem("analyzer-settings");
    const settings = analyzer_settings && JSON.parse(analyzer_settings);
    setSettings(settings);
  }, []);

  const fetchData = async () => {
    localStorage.removeItem("report-data");
    setLoading(true);

    const prompt = `Generat a JSON on the basis of bellow Context dataset 
    The AI assistant will only reply in the following JSON format: 

            { 
              \"charts\": [{ \"title\": string, \"description\": string}, ...]
            }

            Context:
            \"\"\"
            ${filterContextData(reportData)}
            \"\"\"
           
            Instructions:

              1. title, chartType, and data should be same as the context
              2. description should be well summrize and written in easy english in detail
              3. description should be generic and in a article friendly.
              

            The following topics must be considered:
              - Reply only in JSON format
              - if data is not an array of objects, then do not include it
            
                
            Example reply:
              { 
                \"charts\": [{ \"title\": string, \"description\": string }, ...]
              }
    `;

    const config = localStorage.getItem("analyzer-settings");
    const settingData = config && JSON.parse(config);

    if (settingData?.apikey && settings?.mock !== "true") {
      await fetch(OPEN_AI_COMPLETIONS_API, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${settingData.apikey}`,
        },
        method: "POST",
        body: JSON.stringify({
          model: "gpt-3.5-turbo",
          temperature: 0.8,
          messages: [
            { role: "system", content: "You answer questions." },
            { role: "user", content: prompt },
          ],
        }),
        // body: JSON.stringify({
        //   max_tokens: 2000,
        //   model: "text-davinci-003",
        //   temperature: 1,
        //   prompt,
        // }),
      })
        .then((response) => response.json())
        .then((resp) => {
          setLoading(false);
          if (resp?.error) {
            swal("Warning!", resp?.error.message, "warning").then((value) =>
              setLoading(false)
            );
            return;
          }

          const summary = JSON?.parse(resp.choices?.[0]?.message?.content);
          setData(summary);
          localStorage.setItem(
            "report-data",
            resp.choices?.[0]?.message?.content
          );
        })
        .catch((resp) => {
          swal("Warning!", resp?.error.message, "warning").then((value) =>
            setLoading(false)
          );
        });
    } else {
      // alert("mock");
      const summary = JSON?.parse(MockSummary?.choices?.[0]?.message?.content);
      setData(summary);
      localStorage.setItem(
        "report-data",
        MockSummary?.choices?.[0]?.message?.content
      );
      setLoading(false);
    }
  };

  const oldData = () => {
    const summaryRaw = localStorage.getItem("report-data");
    if (summaryRaw) {
      const summary = JSON?.parse(summaryRaw);
      if (summary) setData(summary);
    }
  };

  React.useEffect(() => {
    oldData();
  }, [editable]);

  React.useEffect(() => {
    const reportedData = data?.charts?.map((val: any) => {
      const findInReprtData = reportData?.find(
        (report: any) => report?.title === val?.title
      );
      return { ...findInReprtData, description: val?.description };
    });
    setReportedData(reportedData);
  }, [reportData, data]);

  const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"]; // Add more colors if needed

  const headers = ["x", "y"];

  const saveAsPNG = (index: any) => {
    // const divToCapture = divRef.current;
    const divToCapture = document.getElementById(index);

    if (divToCapture) {
      html2canvas(divToCapture).then((canvas) => {
        const link = document.createElement("a");
        link.href = canvas.toDataURL("image/png");
        link.download = "captured_div.png";
        link.click();
      });
    }
  };

  const foot = [Img2, F3];
  const randomFootIndex = Math.floor(Math.random() * foot.length);
  const randomFootItem = foot[randomFootIndex];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
  }: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        fontSize={12}
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  const CustomLabel = ({ x, y, width, value }: any) => (
    <text
      x={x + width / 2}
      y={y - 10}
      fill="black"
      textAnchor="middle"
      dominantBaseline="middle"
      fontSize={6}
    >
      {formatNumber(value)}
    </text>
  );

  const updateText = (index: number, key: string, text: string) => {
    const getData = data.charts[index][key];
    data.charts[index][key] = text;

    localStorage.setItem("report-data", JSON.stringify(data));
  };

  console.log("==============reportedData++++", reportedData);
  return (
    <>
      <div className="w-100 px-3 in-center-section">
        <div className="w-100 d-flex justify-content-between align-items-center pt-4 mb-4 in-ct-ht">
          <div>
            <h2 className="mb-1">Generate Report</h2>
          </div>
          <div>
            <button
              className="in-btn-cmn me-2 in-btn-pb"
              onClick={() => fetchData()}
            >
              <span className="me-2">
                <Image src={icon_refresh} alt="" />
              </span>{" "}
              Generate
            </button>
          </div>
        </div>
        {/* <Card
          style={{
            padding: 30,
            display: "flex",
            justifyContent: "space-between",
            margin: 30,
          }}
        >
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">
              Generate Report
            </FormLabel>
          </FormControl>

          <Button
            className="analyze"
            variant="contained"
            color="info"
            onClick={() => fetchData()}
          >
            Generate
          </Button>
        </Card> */}
        {loading ? (
          <div className="center">
            <div className="center-button-wrapper">
              <center>
                <div className="loader"></div>
                <br />
                Generating document...
              </center>
            </div>
          </div>
        ) : (
          <>
            {reportedData && (
              <>
                <div className="d-flex gap-2 mb-3">
                  <Button
                    className="analyze"
                    variant="contained"
                    color="info"
                    onClick={() => saveAsPNG(`pdf`)}
                  >
                    Download
                  </Button>
                  <Button
                    className="analyze"
                    variant="outlined"
                    color="info"
                    onClick={() => setEditable(!editable)}
                  >
                    {editable ? "Save" : "Edit Text"}
                  </Button>
                </div>
                <Card style={{ flexGrow: 1 }} id="pdf">
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Image
                        src={headImage}
                        alt="ii"
                        style={{ height: 400, width: "100%" }}
                      />
                      {editable && (
                        <Button
                          className="analyze"
                          variant="outlined"
                          color="warning"
                          onClick={() => {
                            const head = [Img1, Head2, Head3];
                            const randomHeadIndex = Math.floor(
                              Math.random() * head.length
                            );
                            setHeadImage(head[randomHeadIndex]);
                          }}
                          style={{ marginRight: 30, float: "right" }}
                        >
                          Change Image
                        </Button>
                      )}
                    </Grid>

                    {reportedData &&
                      reportedData?.map((data: any, index: number) => {
                        return (
                          <>
                            <Grid item xs={6} style={{ padding: 40 }}>
                              <Typography
                                variant="h5"
                                component="div"
                                style={{
                                  fontSize: 30,
                                  fontWeight: 500,
                                }}
                              >
                                {data?.title}
                              </Typography>
                              <Typography
                                variant="body2"
                                style={{ marginTop: 40, fontSize: 16 }}
                              >
                                {editable ? (
                                  <TextField
                                    id="outlined-multiline-static"
                                    multiline
                                    rows={8}
                                    defaultValue={data?.description}
                                    fullWidth
                                    onChange={(e) =>
                                      updateText(
                                        index,
                                        "description",
                                        e.target.value
                                      )
                                    }
                                    style={{ background: "#8080801c" }}
                                  />
                                ) : (
                                  data?.description
                                )}
                              </Typography>
                              <Card style={{ padding: 30, margin: 30 }}>
                                {data?.data?.length > 6 ? (
                                  <ResponsiveContainer
                                    width="100%"
                                    height={300}
                                  >
                                    <BarChart data={data?.data}>
                                      <CartesianGrid strokeDasharray="3 3" />
                                      <XAxis
                                        dataKey="x"
                                        tick={{ fontSize: 8 }}
                                      />
                                      <YAxis tick={{ fontSize: 10 }} />
                                      <Tooltip />
                                      <Bar
                                        dataKey="y"
                                        fill="#8884d8"
                                        label={
                                          data?.data?.length < 20 && (
                                            <CustomLabel />
                                          )
                                        }
                                      />
                                    </BarChart>
                                  </ResponsiveContainer>
                                ) : (
                                  <>
                                    {data?.data?.length > 2 &&
                                    index % 2 === 0 ? (
                                      <ResponsiveContainer
                                        width="100%"
                                        height={300}
                                      >
                                        <RadarChart data={data?.data}>
                                          <PolarGrid />
                                          <PolarAngleAxis dataKey="x" />
                                          <PolarRadiusAxis
                                            angle={30}
                                            domain={[0, 100]}
                                          />
                                          <Radar
                                            dataKey="y"
                                            fill="#DB4E18"
                                            fillOpacity={0.6}
                                          />
                                        </RadarChart>
                                      </ResponsiveContainer>
                                    ) : (
                                      <ResponsiveContainer
                                        width="100%"
                                        height={300}
                                      >
                                        <PieChart>
                                          <Legend />
                                          <Pie
                                            data={data?.data}
                                            nameKey={"x"}
                                            dataKey={"y"}
                                            cx="50%"
                                            cy="50%"
                                            outerRadius={100}
                                            fill="#8884d8"
                                            labelLine={false}
                                            // label={(entry) =>
                                            //   formatNumber(entry.x)
                                            // }
                                            label={renderCustomizedLabel}
                                          >
                                            {data?.data?.map(
                                              (_: any, index: number) => (
                                                <Cell
                                                  key={`cell-${index}`}
                                                  fill={
                                                    COLORS[
                                                      index % COLORS.length
                                                    ]
                                                  }
                                                />
                                              )
                                            )}
                                          </Pie>
                                          <Tooltip
                                            formatter={(value) =>
                                              formatNumber(value as number)
                                            }
                                          />
                                        </PieChart>
                                      </ResponsiveContainer>
                                    )}
                                  </>
                                )}
                              </Card>
                            </Grid>
                          </>
                        );
                      })}
                  </Grid>
                  <Grid item xs={12}>
                    <Image
                      src={randomFootItem}
                      alt="ii"
                      style={{ height: 400, width: "100%" }}
                    />
                  </Grid>
                </Card>
              </>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default BusinessImprovementList;
