import React, { useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label,
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  ReferenceLine,
  Brush,
  LineChart,
  Line,
} from "recharts";
import { Box, FormControlLabel, Checkbox } from "@mui/material";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { palette } from "../../utils/palette";
import styles from "../../styles/Components.module.scss";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import EventRepeatIcon from "@mui/icons-material/EventRepeat";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "90%",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,

  p: 4,
};

const CustomLabel = ({ x, y, width, value }: any) => (
  <text
    x={x + width / 2}
    y={y - 10}
    fill="black"
    textAnchor="middle"
    dominantBaseline="middle"
    fontSize={12}
  >
    {parseFloat(value).toFixed(1)}
  </text>
);

const BarChartDOM = ({ data, Chart, Unit }: any) => {
  // console.log("===data==>", data);
  const keyNames = data && Object.keys(data[0]);

  // console.log(keyNames);
  const xAxis = keyNames[0];
  const yAxis = keyNames[1];
  return (
    <div style={{ height: "400px", marginTop: "20px" }}>
      <ResponsiveContainer width="100%" height="100%">
        <Chart width={600} height={400} data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey={xAxis}>
            <Label
              value={xAxis}
              position="insideBottom"
              offset={-10}
              fontSize={14}
              fill={palette[0]}
            />
          </XAxis>
          <YAxis dataKey={yAxis}>
            <Label
              value={yAxis}
              position="insideLeft"
              angle={-90}
              offset={0}
              fontSize={14}
              fill={palette[0]}
            />
          </YAxis>
          <Tooltip />
          <Legend />

          <Unit
            dataKey={yAxis}
            fill={"#0288d1"}
            label={data?.length < 20 && <CustomLabel />}
            fontSize={12}
            // maxBarSize={50}
          />
          <Brush dataKey={xAxis} height={30} stroke="#8884d8" />
        </Chart>
      </ResponsiveContainer>
    </div>
  );
};

export default function BarChartData({ filteredData }: any) {
  const [groupData, setGroupData] = useState<boolean>(true);
  const [count, setCount] = useState<boolean>(false);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleGroupDataChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setCount(event.target.checked);
  };

  const keyNames = Object.keys(filteredData[0]);
  // console.log(keyNames);
  const xAxis = keyNames[0];
  const yAxis = keyNames[1];

  // ---------------------------------------------------------------------------------------------------------------------------------

  function groupDataByDateInterval(
    dataArray: any,
    interval: any,
    xProp: string,
    yProp: string
  ) {
    if (!Array.isArray(dataArray) || dataArray.length === 0) {
      return {
        quarterly: [],
        monthly: [],
        yearly: [],
      };
    }

    // Helper function to get the interval string from the date
    const getDateInterval = (date: any, interval: any) => {
      const dateObj = new Date(date);
      switch (interval) {
        case "quarterly":
          return `${dateObj.getUTCFullYear()}-Q${Math.ceil(
            (dateObj.getUTCMonth() + 1) / 3
          )}`;
        case "monthly":
          return `${dateObj.getUTCFullYear()}-${String(
            dateObj.getUTCMonth() + 1
          ).padStart(2, "0")}`;
        case "yearly":
          return `${dateObj.getUTCFullYear()}`;
        default:
          return null;
      }
    };

    // Group the data by date interval
    const groupedData = dataArray.reduce((result, current) => {
      const intervalStr = getDateInterval(current[xProp], interval);
      if (intervalStr) {
        if (!result[intervalStr]) {
          result[intervalStr] = [];
        }
        result[intervalStr].push({
          [xProp]: current[xProp],
          [yProp]: current[yProp],
        });
      }
      return result;
    }, {});

    // Convert grouped data object to an array of arrays
    const resultArrays = Object.entries(groupedData).map(
      ([intervalStr, values]) => ({
        interval: intervalStr,
        data: values,
      })
    );

    return resultArrays;
  }

  const quarterlyData: any = groupDataByDateInterval(
    filteredData,
    "quarterly",
    xAxis,
    yAxis
  );
  const monthlyData: any = groupDataByDateInterval(
    filteredData,
    "monthly",
    xAxis,
    yAxis
  );
  const yearlyData: any = groupDataByDateInterval(
    filteredData,
    "yearly",
    xAxis,
    yAxis
  );

  function calculateSumByInterval(inputData: any) {
    const resultData = inputData?.map((intervalData: any) => {
      const interval = intervalData.interval;
      const sumAdjClose = intervalData.data.reduce(
        (sum: any, dataPoint: any) => sum + dataPoint[yAxis],
        0
      );
      return { label: interval, value: sumAdjClose };
    });

    return { data: resultData };
  }

  function calculateCountByInterval(inputData: any) {
    console.log("inputData", inputData);
    const resultData = inputData?.map((intervalData: any) => {
      const interval = intervalData.interval;
      const sumAdjClose = intervalData?.data.length;
      return { label: interval, value: sumAdjClose };
    });

    return { data: resultData };
  }

  function checkIfValuesAreDates(obj: any, xKey: any) {
    const isXDate = !isNaN(Date.parse(obj[xKey]));
    return isXDate;
  }

  function isDateString(obj: any, xKey: any) {
    if (typeof obj[xKey] !== "string") {
      return false;
    }

    return !isNaN(Date.parse(obj[xKey]));
  }

  // console.log(
  //   "==========================fff====>",
  //   filteredData[0],
  //   checkIfValuesAreDates(filteredData[0], xAxis)
  // );
  const ifEnabled = isDateString(filteredData[0], xAxis);
  return (
    <>
      {filteredData && filteredData?.length > 0 && ifEnabled && (
        <Button
          size="small"
          variant="outlined"
          color="info"
          onClick={handleOpen}
          style={{
            float: "right",
            marginTop: "-1.8rem",
            height: 30,
            minWidth: 30,
            padding: 0,
          }}
        >
          <EventRepeatIcon fontSize="small" />
        </Button>
      )}

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} style={{ overflowY: "auto", height: "90vh" }}>
          <div>
            <FormControlLabel
              control={
                <Checkbox checked={count} onChange={handleGroupDataChange} />
              }
              label="Group Data of X-axis and count coresponding Y-axis values"
            />
          </div>
          {groupData && (
            <Grid container spacing={2}>
              {monthlyData && (
                <Grid item xs={12} style={{ padding: 30 }}>
                  <div style={{ fontSize: 25 }}>Monthly Analysis</div>
                  <BarChartDOM
                    data={
                      count
                        ? calculateCountByInterval(monthlyData)?.data
                        : calculateSumByInterval(monthlyData)?.data
                    }
                    Chart={BarChart}
                    Unit={Bar}
                  />
                </Grid>
              )}
              {quarterlyData && (
                <Grid item xs={6} style={{ padding: 30 }}>
                  <div style={{ fontSize: 25 }}>Quaterly Analysis</div>
                  <BarChartDOM
                    data={
                      count
                        ? calculateCountByInterval(monthlyData)?.data
                        : calculateSumByInterval(quarterlyData)?.data
                    }
                    Chart={BarChart}
                    Unit={Bar}
                  />
                </Grid>
              )}
              {yearlyData && (
                <Grid item xs={6} style={{ padding: 30 }}>
                  <div style={{ fontSize: 25 }}>Yearly Analysis</div>
                  <BarChartDOM
                    data={
                      count
                        ? calculateCountByInterval(monthlyData)?.data
                        : calculateSumByInterval(yearlyData)?.data
                    }
                    Chart={BarChart}
                    Unit={Bar}
                  />
                </Grid>
              )}
              {/* ------------------------------------------------------- */}
              <Grid item xs={12} style={{ padding: 30 }}>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography style={{ fontSize: 20, fontWeight: 500 }}>
                      Month data
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container spacing={2}>
                      {monthlyData?.map((val: any, index: number) => (
                        <Grid item xs={6} style={{ padding: 20 }} key={index}>
                          <div style={{ fontSize: 25 }}>{val?.interval}</div>
                          <BarChartDOM
                            data={val?.data}
                            Chart={LineChart}
                            Unit={Line}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography style={{ fontSize: 20, fontWeight: 500 }}>
                      Quater data
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container spacing={2}>
                      {quarterlyData?.map((val: any, index: number) => (
                        <Grid item xs={6} style={{ padding: 20 }} key={index}>
                          <div style={{ fontSize: 25 }}>{val?.interval}</div>
                          <BarChartDOM
                            data={val?.data}
                            Chart={LineChart}
                            Unit={Line}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography style={{ fontSize: 20, fontWeight: 500 }}>
                      Year data
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container spacing={2}>
                      {yearlyData?.map((val: any, index: number) => (
                        <Grid item xs={6} style={{ padding: 20 }} key={index}>
                          <div style={{ fontSize: 25 }}>{val?.interval}</div>
                          <BarChartDOM
                            data={val?.data}
                            Chart={LineChart}
                            Unit={Line}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </Grid>
            </Grid>
          )}
        </Box>
      </Modal>
    </>
  );
}
