import React, { useEffect, useRef } from "react";
import { IChart, IDataset } from "../../types";
import Button from "@mui/material/Button";
import { PieChart } from "./PieChart";
import { BarChart } from "./BarChart";

import { LineChart } from "./LineChart";
import { ScatterChartDOM } from "./ScatterChart";
import { SimpleRadar } from "./Radar";

import BarChartIcon from "@mui/icons-material/BarChart";
import DonutSmallIcon from "@mui/icons-material/DonutSmall";
import TimelineIcon from "@mui/icons-material/Timeline";
import ScatterPlotIcon from "@mui/icons-material/ScatterPlot";
import MultilineChartIcon from "@mui/icons-material/MultilineChart";

export default function Tab(
  props: React.PropsWithChildren<{
    config: IChart;
    data: IDataset;
    type: string;
  }>
) {
  const [chart, setChart] = React.useState(props.type);
  const [noOfData, setNoOfData] = React.useState(0);

  useEffect(() => {
    // if (noOfData < 4) {
    //   setChart("pieChart");
    // }

    if (noOfData > 20) {
      setChart("lineChart");
    }
    if (noOfData > 8 && chart === "pieChart") {
      setChart("barChart");
    }
  }, [props.data, noOfData]);

  const chartSelect = (noOfData: number) => {
    setNoOfData(noOfData);
  };

  return (
    <>
      <div
        style={{
          display: "flex",
          marginBottom: 5,
          justifyContent: "flex-start",
        }}
      >
        <Button
          size="small"
          variant={chart === "barChart" ? "contained" : "outlined"}
          onClick={() => setChart("barChart")}
          color="info"
          style={{ height: 30, minWidth: 30, padding: 0 }}
        >
          <BarChartIcon fontSize="small" />
        </Button>
        <Button
          size="small"
          variant={chart === "pieChart" ? "contained" : "outlined"}
          color={chart === "pieChart" ? "info" : "inherit"}
          onClick={() => setChart("pieChart")}
          style={{ marginLeft: 10, height: 30, minWidth: 30, padding: 0 }}
          disabled={noOfData > 12}
        >
          <DonutSmallIcon fontSize="small" />
        </Button>
        <Button
          size="small"
          variant={chart === "lineChart" ? "contained" : "outlined"}
          color={chart === "lineChart" ? "info" : "inherit"}
          onClick={() => setChart("lineChart")}
          style={{ marginLeft: 10, height: 30, minWidth: 30, padding: 0 }}
        >
          <TimelineIcon fontSize="small" />
        </Button>
        <Button
          size="small"
          variant={chart === "scatterChart" ? "contained" : "outlined"}
          color={chart === "scatterChart" ? "info" : "inherit"}
          onClick={() => setChart("scatterChart")}
          style={{ marginLeft: 10, height: 30, minWidth: 30, padding: 0 }}
        >
          <ScatterPlotIcon fontSize="small" />
        </Button>
        <Button
          size="small"
          variant={chart === "reader" ? "contained" : "outlined"}
          color={chart === "reader" ? "info" : "inherit"}
          onClick={() => setChart("reader")}
          disabled={noOfData > 12}
          style={{ marginLeft: 10, height: 30, minWidth: 30, padding: 0 }}
        >
          <MultilineChartIcon fontSize="small" />
        </Button>
      </div>

      {chart === "pieChart" && (
        <PieChart
          config={props.config}
          data={props.data}
          chartSelect={chartSelect}
        />
      )}
      {chart === "barChart" && (
        <BarChart
          config={props.config}
          data={props.data}
          chartSelect={chartSelect}
        />
      )}
      {chart === "lineChart" && (
        <LineChart
          config={props.config}
          data={props.data}
          chartSelect={chartSelect}
        />
      )}
      {chart === "scatterChart" && (
        <ScatterChartDOM
          config={props.config}
          data={props.data}
          chartSelect={chartSelect}
        />
      )}
      {chart === "reader" && (
        <SimpleRadar
          config={props.config}
          data={props.data}
          chartSelect={chartSelect}
        />
      )}

      <br />
    </>
  );
}
