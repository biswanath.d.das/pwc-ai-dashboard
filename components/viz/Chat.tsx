import React, { useState, useRef } from "react";
import { Box, Typography, List, ListItem, ListItemText } from "@mui/material";
import MockSuggestion from "../../openai/Mock/suggestion_MOCL.json";
import MockSuggestionResponse from "../../openai/Mock/suggestion_MOCK_full.json";
import Button from "@mui/material/Button";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import Modal from "@mui/material/Modal";
import styles from "../../styles/Components.module.scss";
import TextField from "@mui/material/TextField";
import { useChatData } from "../../pages/_app";
import ChartContent from "./ChatContent";
import swal from "sweetalert";
import { OPEN_AI_COMPLETIONS_API } from "../../config";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: 700,
  overflow: "auto",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const BusinessImprovementList: React.FC<any> = ({ chartData, title }: any) => {
  const [data, setData] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [chatVal, setChatVal] = React.useState<any>(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [question, setQuestion] = useState("");

  const { chatData } = useChatData();

  const createPrompt = (question: string | null, chatData: any) => {
    return `Use the below Dataset and summary as a context to answer the subsequent question. If the answer cannot be found, write "I don't know."

        Dataset:
        \"\"\"
        ${JSON.stringify(chatData)}
        \"\"\"

        Summary:
        \"\"\"
        ${localStorage.getItem("data-summary")}
        \"\"\"

        Question: ${question}?
        
        The following topics must be considered:
        - if asked for example top 3/4/.. kpi, then say the kpi having gretest total value in the asc oreder.
        - give answer in a full sentence
        - give a bordered html table of comrapreson when required (example : top 3 kpi )
        - Reply only in valid HTML code with the answer
        - on the Dataset assume [
            {
                "title": "kpi1 by kpi2",
                "data": [
                    {
                        "x": 2,
                        "y": 2619
                    }
                ]
            },
            ...
            {
                "title": "Total Quantity Ordered",
                "data": "35.0K"
            },
            ...
        ] here under x = kpi2, y = kpi1

        Example reply:
            <p>Answer...</>
        `;
  };

  const fetchData = async () => {
    const val = {
      user: "client",
      text: question,
    };
    const chartLocal = localStorage.getItem("chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      chat.push(val);
      localStorage.setItem("chat", JSON.stringify(chat));
    } else {
      localStorage.setItem("chat", JSON.stringify([val]));
    }

    setLoading(true);
    const config = localStorage.getItem("analyzer-settings");
    const settingData = config && JSON.parse(config);

    if (settingData?.apikey && open) {
      await fetch(OPEN_AI_COMPLETIONS_API, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${settingData.apikey}`,
        },
        method: "POST",
        body: JSON.stringify({
          model: "gpt-3.5-turbo",
          temperature: 1,
          messages: [
            { role: "system", content: "You answer questions." },
            { role: "user", content: createPrompt(question, chatData) },
          ],
        }),
      })
        .then((response) => response.json())
        .then((resp) => {
          const chartLocal = localStorage.getItem("chat");
          const chat = chartLocal && JSON.parse(chartLocal);
          const val = {
            user: "AI",
            text: resp?.choices[0]?.message?.content,
          };
          chat.push(val);
          localStorage.setItem("chat", JSON.stringify(chat));

          //   const jsonObject = JSON.parse(resp.choices?.[0]?.text);
          setData(resp?.choices[0]?.message?.content);
          setQuestion("");
          //   console.log("====formated=======>", jsonObject);
          setLoading(false);

          if (resp?.error) {
            swal("Warning!", resp?.error.message, "warning").then((value) =>
              setLoading(false)
            );
            return;
          }
        })
        .catch((resp) => {
          swal("Warning!", resp?.error.message, "warning").then((value) =>
            setLoading(false)
          );
        });
    }
  };

  React.useEffect(() => {
    const chartLocal = localStorage.getItem("chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      setChatVal(chat);
    }
  }, [loading]);

  return (
    <>
      <section className="w-100 d-flex h-100 in-center-section flex-column px-4">
        <div className="w-100 d-flex justify-content-between align-items-center px-3 pt-4 mb-2 in-ct-ht">
          <div>
            <h2 className="mb-1">Ask questions</h2>
          </div>
          {loading ? (
            <div className="center-button-wrapper">
              <center>
                <div className="loader"></div>
                <br />
                Finding data...
              </center>
            </div>
          ) : (
            <>
              {/* <Typography variant="h4" gutterBottom>
                {JSON.stringify(chatData)}
              </Typography> */}
              {chatVal && <ChartContent chatVal={chatVal} loading={loading} />}
            </>
          )}
          <div
            style={{
              bottom: 20,
              position: "absolute",
              width: "90%",
            }}
          >
            <TextField
              id="outlined-basic"
              label="Question"
              variant="outlined"
              fullWidth
              value={question}
              onChange={(e: any) => setQuestion(e.target.value)}
            />
            <Button
              variant="contained"
              onClick={() => fetchData()}
              style={{ marginTop: 10, background: "#0288d1" }}
              fullWidth
            >
              Ask
            </Button>
          </div>
        </div>
      </section>
    </>
  );
};

export default BusinessImprovementList;
