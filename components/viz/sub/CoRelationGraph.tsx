import React, { useEffect } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import BarChartIcon from "@mui/icons-material/BarChart";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "95%",
  height: "90vh",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  overflow: "auto",
};

interface DataItem {
  DATA: string;
  QUANTITYORDERED: string;
}

interface Props {
  data: DataItem[];
  graph: string[];
}

interface ChildComponentProps {
  iniStateData: any;
  graph: any;
}

const App: React.FC<ChildComponentProps> = ({ iniStateData, graph }: any) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [record, setRecord] = React.useState(iniStateData?.length);
  const [stateData, setStateData] = React.useState(iniStateData);

  function getFirstTwoDataItems(dataArray: any, record: any): DataItem[] {
    return dataArray?.slice(0, record);
  }

  React.useEffect(() => {
    const filterData = getFirstTwoDataItems([...iniStateData], record);
    setStateData(filterData);
  }, [iniStateData, record]);

  console.log("---graph->", graph);
  return (
    <div>
      <Button
        onClick={handleOpen}
        variant="outlined"
        color="warning"
        endIcon={<BarChartIcon />}
      >
        View
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Chart
          </Typography>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="No of records"
              variant="outlined"
              value={record}
              onChange={(e) => setRecord(e.target.value)}
            />
          </Box>
          <div>
            <ResponsiveContainer width="100%" height={500}>
              <LineChart
                data={stateData}
                margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey={graph[0]} />
                <YAxis />
                <Tooltip />
                <Legend />
                {graph?.slice(1).map((key: any, index: number) => (
                  <Line
                    key={index}
                    type="monotone"
                    dataKey={key}
                    stroke={`#${(Math.random() * 0xfffff * 1000000)
                      .toString(16)
                      .slice(0, 6)}`}
                  />
                ))}
              </LineChart>
            </ResponsiveContainer>
          </div>
        </Box>
      </Modal>
    </div>
  );
};

export default App;
