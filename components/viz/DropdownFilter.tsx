import React from "react";
import { IDataset, IFilter } from "../../types";
import styles from "../../styles/Components.module.scss";
import Image from "next/image";
import icon_filter from "../../images/icon_filter.svg";

function removeFilterText(text: string) {
  const filterTextElement = document.getElementById("filterText");
  const updatedText = text.replace("Filter by ", "");
  return updatedText;
}

export function DropdownFilter(
  props: React.PropsWithChildren<{
    config: IFilter;
    data: IDataset;
    onChange?: (value: string) => void;
    value?: string;
  }>
) {
  const values = React.useMemo(() => {
    return props.data
      .map((row) => row[props.config.column])
      .filter((x, i, arr) => arr.indexOf(x) === i)
      .filter((x) => x)
      .sort((a, b) => (a > b ? 1 : -1));
  }, [props.config, props.data]);

  const handleChange = React.useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      props.onChange?.(e.target.value);
    },
    [props.onChange]
  );

  return (
    <div style={{ display: "grid" }}>
      <label style={{ fontSize: 15 }}>
        {removeFilterText(props.config.title)}
      </label>
      <select
        value={props.value}
        onChange={handleChange}
        style={{ fontSize: 14 }}
      >
        <option key={"None"} value="">
          -- Select --
        </option>
        {values.map((value) => (
          <option key={value} value={value}>
            {value}
          </option>
        ))}
      </select>
    </div>
  );
}
