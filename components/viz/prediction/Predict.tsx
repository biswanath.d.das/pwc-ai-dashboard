interface DataPoint {
  [key: string]: number | string;
}

interface FutureProjectionData {
  [key: string]: number | string;
}

export function linearRegression(
  data: any,
  futurePoints: number,
  xKey: string,
  yKey: string
): FutureProjectionData[] {
  const n = data.length;
  let sumX = 0;
  let sumY = 0;
  let sumXY = 0;
  let sumX2 = 0;

  for (const point of data) {
    const xVal = parseFloat(point[xKey]);
    sumX += xVal;
    sumY += point[yKey];
    sumXY += xVal * point[yKey];
    sumX2 += xVal * xVal;
  }

  const slope = (n * sumXY - sumX * sumY) / (n * sumX2 - sumX * sumX);
  const intercept = (sumY - slope * sumX) / n;

  const futureProjection: FutureProjectionData[] = [];
  for (let i = 1; i <= futurePoints; i++) {
    const futureX = (n + i).toString();
    const projectedY = slope * (n + i) + intercept;
    futureProjection.push({ x: futureX, y: projectedY });
  }

  return futureProjection;
}

export function polynomialRegression(
  data: any,
  futurePoints: number,
  degree: number,
  xKey: string,
  yKey: string
): FutureProjectionData[] {
  const coefficients = polynomialFit(
    data.map((point: any) => parseFloat(point[xKey])),
    data.map((point: any) => point[yKey]),
    degree
  );

  const futureProjection: FutureProjectionData[] = [];
  for (let i = 1; i <= futurePoints; i++) {
    const futureX = (data.length + i).toString();
    const projectedY = evaluatePolynomial(coefficients, data.length + i);
    futureProjection.push({ x: futureX, y: projectedY });
  }

  return futureProjection;
}

export function evaluatePolynomial(coefficients: number[], x: number): number {
  let result = 0;
  for (let i = 0; i < coefficients.length; i++) {
    result += coefficients[i] * Math.pow(x, i);
  }
  return result;
}

export function polynomialFit(
  x: number[],
  y: number[],
  degree: number
): number[] {
  const matrix: number[][] = [];
  const n = x.length;

  for (let i = 0; i <= degree; i++) {
    const row: number[] = [];
    for (let j = 0; j <= degree; j++) {
      let sum = 0;
      for (let k = 0; k < n; k++) {
        sum += Math.pow(x[k], i + j);
      }
      row.push(sum);
    }
    matrix.push(row);
  }

  const augmentedMatrix: number[][] = [];
  for (let i = 0; i <= degree; i++) {
    augmentedMatrix.push(matrix[i].concat([sumPower(x, y, i)]));
  }

  const coefficients = gaussElimination(augmentedMatrix);

  return coefficients;
}

export function sumPower(x: number[], y: number[], power: number): number {
  let sum = 0;
  for (let i = 0; i < x.length; i++) {
    sum += Math.pow(x[i], power) * y[i];
  }
  return sum;
}

export function gaussElimination(matrix: number[][]): number[] {
  // Gauss Elimination algorithm to solve the system of linear equations
  // (augmented matrix)
  // Returns the solution (coefficients)
  // Assumes that the matrix is square

  const n = matrix.length;
  for (let i = 0; i < n; i++) {
    // Search for maximum in this column
    let maxRow = i;
    for (let k = i + 1; k < n; k++) {
      if (Math.abs(matrix[k][i]) > Math.abs(matrix[maxRow][i])) {
        maxRow = k;
      }
    }

    // Swap maximum row with current row (column by column)
    for (let k = i; k < n + 1; k++) {
      const tmp = matrix[maxRow][k];
      matrix[maxRow][k] = matrix[i][k];
      matrix[i][k] = tmp;
    }

    // Make all rows below this one 0 in current column
    for (let k = i + 1; k < n; k++) {
      const factor = matrix[k][i] / matrix[i][i];
      for (let j = i; j < n + 1; j++) {
        matrix[k][j] -= factor * matrix[i][j];
      }
    }
  }

  // Solve equation Ax=b for an upper triangular matrix A
  const coefficients: number[] = new Array(n);
  for (let i = n - 1; i >= 0; i--) {
    coefficients[i] = matrix[i][n] / matrix[i][i];
    for (let k = i - 1; k >= 0; k--) {
      matrix[k][n] -= matrix[k][i] * coefficients[i];
    }
  }

  return coefficients;
}

export function timeSeriesAnalysis(
  data: DataPoint[],
  futurePoints: number,
  windowSize: number,
  yKey: string
): FutureProjectionData[] {
  const movingAverage: FutureProjectionData[] = [];

  for (let i = 0; i < data.length; i++) {
    if (i >= windowSize - 1) {
      let sum = 0;
      for (let j = i; j >= i - (windowSize - 1); j--) {
        sum += parseFloat(data[j][yKey].toString());
      }
      const average = sum / windowSize;
      movingAverage.push({ ...data[i], [yKey]: average });
    }
  }

  const lastDataPoint = data[data.length - 1];
  let lastY = parseFloat(lastDataPoint[yKey].toString());

  const futureProjection: FutureProjectionData[] = [];
  for (let i = 1; i <= futurePoints; i++) {
    const futureX = (data.length + i).toString();
    const projectedY =
      2 * lastY -
      parseFloat(movingAverage[movingAverage.length - 1][yKey].toString());
    futureProjection.push({ ...lastDataPoint, [yKey]: projectedY });
    lastY = projectedY;
  }

  return futureProjection;
}
