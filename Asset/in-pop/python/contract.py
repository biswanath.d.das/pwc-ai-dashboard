# Install the required libraries
# pip install PyPDF2
# pip install transformers

import PyPDF2
from transformers import pipeline

# Step 1: PDF Extraction


def extract_text_from_pdf(pdf_path):
    with open(pdf_path, "rb") as file:
        reader = PyPDF2.PdfFileReader(file)
        text = ""
        for page_num in range(reader.getNumPages()):
            page = reader.getPage(page_num)
            text += page.extractText()
    return text

# Step 2: Text Preprocessing (Optional)

# Step 3: Key Clause Identification


def find_key_clauses(text):
    classifier = pipeline("text-classification",
                          model="path_to_fine_tuned_model")
    # Replace "path_to_fine_tuned_model" with the path to your fine-tuned model
    results = classifier(text)
    return results

# Step 4-7: Post-processing and Selecting Key Clauses (Optional)


# Example usage
pdf_path = "path_to_your_pdf_contract.pdf"
contract_text = extract_text_from_pdf(pdf_path)
key_clause_results = find_key_clauses(contract_text)

# Print the sentences classified as key clauses with high probabilities
threshold = 0.8
for result in key_clause_results:
    if result["score"] > threshold:
        print(result["label"], result["score"], result["sentence"])
