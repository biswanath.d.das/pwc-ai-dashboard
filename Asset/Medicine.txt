[
    {
        "name": "Metformin",
        "compositions": [
            "Metformin hydrochloride",
            "Povidone",
            "Magnesium stearate"
        ]
    },
    {
        "name": "Glimepiride",
        "compositions": [
            "Glimepiride",
            "Lactose monohydrate",
            "Croscarmellose sodium",
            "Magnesium stearate"
        ]
    },
    {
        "name": "Pioglitazone",
        "compositions": [
            "Pioglitazone hydrochloride",
            "Lactose monohydrate",
            "Povidone",
            "Magnesium stearate"
        ]
    },
    {
        "name": "Sitagliptin",
        "compositions": [
            "Sitagliptin phosphate monohydrate",
            "Microcrystalline cellulose",
            "Lactose monohydrate",
            "Croscarmellose sodium",
            "Magnesium stearate"
        ]
    }
]



-----------------------------------------------------


var myHeaders = new Headers();
myHeaders.append("authority", "soumenopenai.openai.azure.com");
myHeaders.append("accept", "*/*");
myHeaders.append("accept-language", "en-GB,en-US;q=0.9,en;q=0.8");
myHeaders.append("api-key", "3a5a6eba4d2546558d3fa749ef9fb5ce");
myHeaders.append("authorization", "Bearer Bearer a8fe99cb6b354a06913f189536cdf8fc");
myHeaders.append("content-type", "application/json");
myHeaders.append("origin", "http://localhost:3000");
myHeaders.append("referer", "http://localhost:3000/");
myHeaders.append("sec-ch-ua", "\"Not/A)Brand\";v=\"99\", \"Google Chrome\";v=\"115\", \"Chromium\";v=\"115\"");
myHeaders.append("sec-ch-ua-mobile", "?0");
myHeaders.append("sec-ch-ua-platform", "\"Windows\"");
myHeaders.append("sec-fetch-dest", "empty");
myHeaders.append("sec-fetch-mode", "cors");
myHeaders.append("sec-fetch-site", "cross-site");
myHeaders.append("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36");

var raw = JSON.stringify({
  "messages": [
    {
      "role": "system",
      "content": "You are a polite insurance guide. Your name is Jan"
    },
    {
      "role": "user",
      "content": "\n          suggest a a list of medicine and itz compositions for the treatment of blood suger\n\n          Poins must be consider for reply:\n          - Reply only a json for only\n\n          Expeted output format\n          [\n            {\n              name:\"medicine 1\",\n              compositions:[\"c1\", \"c2\", \"c3\"]\n            },\n            {\n              name:\"medicine 2\",\n              compositions:[\"c1\", \"c2\", \"c3\"]\n            },\n            ....\n          ]\n        "
    }
  ],
  "max_tokens": 800,
  "temperature": 0.8,
  "frequency_penalty": 0,
  "presence_penalty": 0,
  "top_p": 0.95,
  "stop": null
});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("https://soumenopenai.openai.azure.com/openai/deployments/gpt-35-turbo/chat/completions?api-version=2023-03-15-preview", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));